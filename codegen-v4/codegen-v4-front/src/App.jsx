import {Switch} from "react-router-dom";
import LoginForm from "./auth/LoginForm";
import Logout from "./auth/Logout";
import AuthWrapper from "./auth/AuthWrapper";
import {ALL_USERS, AUTUMN2020_USERS, AUTUMN2020_VAR1_USERS, AUTUMN2020_VAR2_USERS, AUTUMN2020_VAR3_USERS, SPRING2021_USERS} from "./service/security";
import AccessContext from "./page/accesscontext/AccessContext";
import NavigationPanel from "./page/navbar/NavigationPanel";
import MainPageView from "./page/main/MainPageView";
import Autumn2020Landing from "./page/year/2020/autumn/landing/Landing";
import Spring2021Landing from "./page/year/2021/spring/landing/Landing";
import Spring2021InputFileNameForm from "./page/year/2021/spring/inputfileform/InputFileForm";
import Spring2021ActionsOverview from "./page/year/2021/spring/actionsoverview/ActionsOverview";
import Spring2021FetchExerciseInfo from "./page/year/2021/spring/exerciseinfo/FetchExerciseInfo"
import Autumn2020Var1InputFileForm from "./page/year/2020/autumn/inputfileform/var1/Autumn2020Var1InputFileForm";
import Autumn2020Var2InputFileForm from "./page/year/2020/autumn/inputfileform/var2/Autumn2020Var2InputFileForm";
import Autumn2020Var3ExtraInfoForm from "./page/year/2020/autumn/inputfileform/var3/Autumn2020Var3ExtraInfoForm";
import Autumn2020Var1ActionsOverview from "./page/year/2020/autumn/actionsoverview/var1/Autumn2020Var1ActionsOverview";
import Autumn2020Var2ActionsOverview from "./page/year/2020/autumn/actionsoverview/var2/Autumn2020Var2ActionsOverview";
import Autumn2020Var3ActionsOverview from "./page/year/2020/autumn/actionsoverview/var3/Autumn2020Var3ActionsOverview";
import FetchAutumn2020Var1ExerciseInfo from "./page/year/2020/autumn/exerciseinfo/var1/FetchAutumn2020Var1ExerciseInfo";
import FetchAutumn2020Var2ExerciseInfo from "./page/year/2020/autumn/exerciseinfo/var2/FetchAutumn2020Var2ExerciseInfo";
import FetchAutumn2020Var3ExerciseInfo from "./page/year/2020/autumn/exerciseinfo/var3/FetchAutumn2020Var3ExerciseInfo";

const App = () => {
  return (
    <Switch>
      <AuthWrapper path="/login" component={LoginForm} rolesAllowed={ALL_USERS} />
      <AuthWrapper path="/logout" component={Logout} rolesAllowed={ALL_USERS} />
      <div>
        <AccessContext />
        <NavigationPanel />
        <AuthWrapper path="/" component={MainPageView} rolesAllowed={ALL_USERS} />
        <AuthWrapper
          path="/year/2020/autumn/menu"
          component={Autumn2020Landing}
          rolesAllowed={AUTUMN2020_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var1/actions-overview/:inputFileName"
          component={Autumn2020Var1ActionsOverview}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var2/actions-overview/:inputFileName"
          component={Autumn2020Var2ActionsOverview}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var3/actions-overview/:inputFileName"
          component={Autumn2020Var3ActionsOverview}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var1/input-file/select"
          component={Autumn2020Var1InputFileForm}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var2/input-file/select"
          component={Autumn2020Var2InputFileForm}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var3/extra-info"
          component={Autumn2020Var3ExtraInfoForm}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var1/exercise-info/:inputFileName"
          component={FetchAutumn2020Var1ExerciseInfo}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var2/exercise-info/:inputFileName"
          component={FetchAutumn2020Var2ExerciseInfo}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <AuthWrapper
          path="/year/2020/autumn/var3/exercise-info/:inputFileName"
          component={FetchAutumn2020Var3ExerciseInfo}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <AuthWrapper
          path="/year/2021/spring/menu"
          component={Spring2021Landing}
          rolesAllowed={SPRING2021_USERS}
        />
        <AuthWrapper
          path="/year/2021/spring/input-file/select"
          component={Spring2021InputFileNameForm}
          rolesAllowed={SPRING2021_USERS}
        />
        <AuthWrapper
          path="/year/2021/spring/actions-overview/:inputFileName"
          component={Spring2021ActionsOverview}
          rolesAllowed={SPRING2021_USERS}
        />
        <AuthWrapper
          path="/year/2021/spring/exercise-info/:inputFileName"
          component={Spring2021FetchExerciseInfo}
          rolesAllowed={SPRING2021_USERS}
        />
      </div>
    </Switch>
  );
};

export default App;