import {checkUserAccess} from "../../../service/security";
import MainPageButton from "../button/MainPageButton";

const ButtonAuthWrapper = props => {
  const path = props.path;
  const text = props.text;
  const isServiceEnabled = props.isServiceEnabled;
  const rolesAllowed = props.rolesAllowed;
  const className = props.className;
  const onClickEvent = props.onClickEvent;

  const hasAccess = checkUserAccess(rolesAllowed);

  if (!hasAccess) {
    return <></>
  }

  return <MainPageButton
    path={path}
    text={text}
    isServiceEnabled={isServiceEnabled}
    className={className}
    onClickEvent={onClickEvent}
  />
};

export default ButtonAuthWrapper;