import {Container, Nav, Navbar} from "react-bootstrap";
import {checkIfUserHasToken, getCurrentUser} from "../../service/security";
import {Redirect} from "react-router-dom";

const NavigationPanel = () => {
  const hasToken = checkIfUserHasToken();

  if (!hasToken) {
    return <Redirect to={{pathname: "/login", state: {errorInfo: "Пожалуйста, авторизуйтесь"}}} />
  }

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Codegen</Navbar.Brand>
        <Navbar.Brand>{"Пользователь: " + getCurrentUser()}</Navbar.Brand>
        <Nav className="text-end">
          <Nav.Link href="/">На главную</Nav.Link>
          <Nav.Link href="/logout">Выйти</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavigationPanel;