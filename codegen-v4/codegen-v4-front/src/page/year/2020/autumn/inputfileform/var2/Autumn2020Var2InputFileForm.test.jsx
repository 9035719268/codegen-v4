import {render, unmountComponentAtNode} from "react-dom";
import Autumn2020Var2InputFileForm from "./Autumn2020Var2InputFileForm";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderForm", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=false"
  };

  render(<Autumn2020Var2InputFileForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Выберите файл наблюдений");
});