import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const IonCoefficientsView = props => {
  let coefficients = null;

  if (props.coefficients !== undefined) {
    coefficients = getValuesDividedByComma(props.coefficients);
  }

  return (
    <div>Массив {props.coefficientsName}-коэффициентов:
      <span className="inf">{coefficients === null ? "Загрузка..." : coefficients}</span>
    </div>
  );
};

export default IonCoefficientsView;