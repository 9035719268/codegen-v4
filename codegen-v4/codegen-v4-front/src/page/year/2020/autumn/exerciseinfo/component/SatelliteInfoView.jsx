const SatelliteInfoView = props => {
  let satelliteNumber = null;

  if (props.number !== undefined) {
    satelliteNumber = props.number;
  }

  return (
    <div>Номер {props.order} спутника:
      <span className="inf">{satelliteNumber === null ? "Загрузка..." : satelliteNumber}</span>
    </div>
  );
};

export default SatelliteInfoView;