import {render, unmountComponentAtNode} from "react-dom";
import InputFileNameView from "./InputFileNameView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFileNameView", () => {
  render(<InputFileNameView inputFileName="POTS_6hours.dat" />, container);

  expect(container.textContent).toBe("Название файла:POTS_6hours.dat");
});