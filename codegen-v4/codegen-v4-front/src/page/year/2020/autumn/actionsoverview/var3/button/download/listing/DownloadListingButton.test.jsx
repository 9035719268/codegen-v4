import {render, unmountComponentAtNode} from "react-dom";
import DownloadListingButton from "./DownloadListingButton";
import {downloadGraphDrawerJava, downloadMainCpp, downloadMainJava, downloadMainPython} from "../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const extraInfo = {
  latitude: 52.5,
  longitude: 162,
  satelliteNumber: 7
};

it("shouldRenderCppDownloadListingButton", () => {
  const preferenceInfo = {
    lang: "cpp",
    file: "file",
    oop: "withoop"
  };

  render(
    <DownloadListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
});

it("shouldRenderJavaDownloadListingButtons", () => {
  const preferenceInfo = {
    lang: "java",
    file: "file",
    oop: "withoop"
  };

  render(
    <DownloadListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainJava);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(downloadGraphDrawerJava);
});

it("shouldRenderPythonDownloadListingButton", () => {
  const preferenceInfo = {
    lang: "python",
    file: "file",
    oop: "withoop"
  };

  render(
    <DownloadListingButton
      preferenceInfo={preferenceInfo}
      extraInfo={extraInfo}
    />, container
  );

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainPython);
});
