import {render, unmountComponentAtNode} from "react-dom";
import DownloadChartsButton from "./DownloadChartsButton";
import {downloadCharts} from "../../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadChartsButton", () => {
  render(<DownloadChartsButton inputFileName="POTS_6hours.dat" />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadCharts);
});