import {downloadGraphDrawerJava, downloadMainJava} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var2DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var2/path";
import {javaGraphDrawerFileName, javaMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadJavaListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getAutumn2020Var2DownloadListingPath(preferenceInfo, inputFileName, javaMainFileName);
  const graphDrawerPath = getAutumn2020Var2DownloadListingPath(preferenceInfo, inputFileName, javaGraphDrawerFileName);

  return (
    <span>
      <DownloadButton path={mainPath} fileName={javaMainFileName} label={downloadMainJava} />
      <DownloadButton path={graphDrawerPath} fileName={javaGraphDrawerFileName} label={downloadGraphDrawerJava} />
    </span>
  );
};

export default DownloadJavaListingButton;