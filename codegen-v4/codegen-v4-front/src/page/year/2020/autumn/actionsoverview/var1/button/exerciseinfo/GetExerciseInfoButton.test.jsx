import {render, unmountComponentAtNode} from "react-dom";
import GetExerciseInfoButton from "./GetExerciseInfoButton";
import {getExerciseInfo} from "../../../../../../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  render(<GetExerciseInfoButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(getExerciseInfo);
});

it("shouldRenderVar1GetExerciseInfoButton", () => {
  render(<GetExerciseInfoButton varNumber="1" inputFileName="POTS_6hours.dat" />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2020/autumn/var1/exercise-info/POTS_6hours.dat");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(getExerciseInfo);
});