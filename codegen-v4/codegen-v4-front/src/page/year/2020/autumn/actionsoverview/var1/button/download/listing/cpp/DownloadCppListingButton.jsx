import {downloadMainCpp} from "../../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var1DownloadListingPath} from "../../../../../../../../../../config/path/back/autumn2020/task/var1/path";
import {cppMainFileName} from "../../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../../service/api/download/DownloadButton";

const DownloadCppListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getAutumn2020Var1DownloadListingPath(preferenceInfo, inputFileName, cppMainFileName);

  return (
    <DownloadButton path={mainPath} fileName={cppMainFileName} label={downloadMainCpp} />
  );
};

export default DownloadCppListingButton;