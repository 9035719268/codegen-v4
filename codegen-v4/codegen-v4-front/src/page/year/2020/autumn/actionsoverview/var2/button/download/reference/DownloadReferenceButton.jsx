import {autumn2020DownloadReferencePath} from "../../../../../../../../../config/path/back/autumn2020/task/resource/path";
import {downloadReference} from "../../../../../../../../../config/label/overviewbutton/label";
import {referenceFileName} from "../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";

const DownloadReferenceButton = () => {
  return (
    <DownloadButton
      path={autumn2020DownloadReferencePath}
      fileName={referenceFileName}
      label={downloadReference}
    />
  );
};

export default DownloadReferenceButton;