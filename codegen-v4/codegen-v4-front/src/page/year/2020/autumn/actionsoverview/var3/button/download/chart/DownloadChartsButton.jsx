import {downloadCharts} from "../../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var3DownloadChartsPath} from "../../../../../../../../../config/path/back/autumn2020/task/var3/path";
import {chartsFileName} from "../../../../../../../../../config/label/outputfilename/label";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";

const DownloadChartsButton = props => {
  const extraInfo = props.extraInfo;
  const chartsPath = getAutumn2020Var3DownloadChartsPath(extraInfo);

  return (
    <DownloadButton path={chartsPath} fileName={chartsFileName} label={downloadCharts} />
  );
};

export default DownloadChartsButton;