import {getExerciseInfo} from "../../../../../../../../config/label/overviewbutton/label";
import {getAutumn2020Var2ExerciseInfoPagePath} from "../../../../../../../../config/path/front/autumn2020/var2/path";

const GetExerciseInfoButton = props => {
  const inputFileName = props.inputFileName;
  const exerciseInfoPath = getAutumn2020Var2ExerciseInfoPagePath(inputFileName);

  return (
    <a href={exerciseInfoPath}>
      <button className="overview" type="submit">{getExerciseInfo}</button>
    </a>
  );
};

export default GetExerciseInfoButton;