import {AUTUMN2020_VAR1_USERS, AUTUMN2020_VAR2_USERS, AUTUMN2020_VAR3_USERS} from "../../../../../../../service/security";
import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {autumn2020Var1ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var1/path";
import {autumn2020Var2ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var2/path";
import {autumn2020Var3ExerciseInfoFormPath} from "../../../../../../../config/path/front/autumn2020/var3/path";
import {java} from "../../../../../../../config/label/lang/label";
import {fileWithOop, manualWithOop} from "../../../../../../../config/label/tablebutton/autumn2020/label";

const JavaTableRow = () => {
  return (
    <tr>
      <th scope="row">{java}</th>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="java"
          file="false"
          oop="true"
          text={manualWithOop}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var1ExerciseInfoFormPath}
          lang="java"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR1_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="java"
          file="false"
          oop="true"
          text={manualWithOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var2ExerciseInfoFormPath}
          lang="java"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR2_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="java"
          file="false"
          oop="true"
          text={manualWithOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
        <TableQueryAuthWrapper
          path={autumn2020Var3ExerciseInfoFormPath}
          lang="java"
          file="true"
          oop="true"
          text={fileWithOop}
          rolesAllowed={AUTUMN2020_VAR3_USERS}
        />
      </td>
    </tr>
  );
};

export default JavaTableRow;