const TableQuery = props => {
  const path = props.path;
  const text = props.text;

  const preferenceInfo = props.preferenceInfo;
  const lang = preferenceInfo.lang;
  const file = preferenceInfo.file;
  const oop = preferenceInfo.oop;

  return (
    <form action={path} method="get" target="_blank">
      <input type="hidden" name="lang" value={lang} />
      <input type="hidden" name="file" value={file} />
      <input type="hidden" name="oop" value={oop} />
      <button type="submit" className="btn btn-link landing">{text}</button>
    </form>
  );
};

export default TableQuery;