import {var1, var2, var3} from "../../../../../../config/label/var/label";

const TableHeader = () => {
  return (
    <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">{var1}</th>
      <th scope="col">{var2}</th>
      <th scope="col">{var3}</th>
    </tr>
    </thead>
  );
};

export default TableHeader;