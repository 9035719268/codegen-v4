import {getExerciseInfo} from "../../../../../config/label/overviewbutton/label";
import {getSpring2021ExerciseInfoPagePath} from "../../../../../config/path/front/spring2021/path";

const GetExerciseInfoButton = props => {
  const path = getSpring2021ExerciseInfoPagePath(props.inputFileName);

  return (
    <a href={path}>
      <button className="overview" type="submit">{getExerciseInfo}</button>
    </a>
  );
};

export default GetExerciseInfoButton;