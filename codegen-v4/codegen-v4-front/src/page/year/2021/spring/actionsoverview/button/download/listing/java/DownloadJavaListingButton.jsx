import {downloadGraphDrawerJava, downloadMainJava} from "../../../../../../../../../config/label/overviewbutton/label";
import {getSpring2021DownloadListingPath} from "../../../../../../../../../config/path/back/spring2021/task/path";
import DownloadButton from "../../../../../../../../../service/api/download/DownloadButton";
import {javaGraphDrawerFileName, javaMainFileName} from "../../../../../../../../../config/label/outputfilename/label";

const DownloadJavaListingButton = props => {
  const preferenceInfo = props.preferenceInfo;
  const inputFileName = props.inputFileName;

  const mainPath = getSpring2021DownloadListingPath(preferenceInfo, inputFileName, javaMainFileName);
  const graphDrawerPath = getSpring2021DownloadListingPath(preferenceInfo, inputFileName, javaGraphDrawerFileName);

  return (
    <span>
      <DownloadButton path={mainPath} fileName={javaMainFileName} label={downloadMainJava} />
      <DownloadButton path={graphDrawerPath} fileName={javaGraphDrawerFileName} label={downloadGraphDrawerJava} />
    </span>
  );
};

export default DownloadJavaListingButton;