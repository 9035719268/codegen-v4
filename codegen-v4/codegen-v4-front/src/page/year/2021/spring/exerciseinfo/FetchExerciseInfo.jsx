import {useEffect, useState} from "react";
import ExerciseInfo from "./ExerciseInfo";
import getSatellitesData from "../../../../../service/api/spring2021/getSatellitesData";

const FetchExerciseInfo = props => {
  const inputFileName = props.match.params.inputFileName;
  const [satellite1Data, setSatellite1Data] = useState({});
  const [satellite2Data, setSatellite2Data] = useState({});
  const [satellite3Data, setSatellite3Data] = useState({});

  useEffect(() => {
    getSatellitesData(inputFileName).then(satellitesData => {
      const satellite1 = satellitesData.satellite1;
      setSatellite1Data({
        number: satellite1.number,
        md: satellite1.md,
        td: satellite1.td,
        mw: satellite1.mw,
        tw: satellite1.tw,
        elevation: satellite1.elevation
      });
      const satellite2 = satellitesData.satellite2;
      setSatellite2Data({
        number: satellite2.number,
        md: satellite2.md,
        td: satellite2.td,
        mw: satellite2.mw,
        tw: satellite2.tw,
        elevation: satellite2.elevation
      });
      const satellite3 = satellitesData.satellite3;
      setSatellite3Data({
        number: satellite3.number,
        md: satellite3.md,
        td: satellite3.td,
        mw: satellite3.mw,
        tw: satellite3.tw,
        elevation: satellite3.elevation
      });
    });
  }, [inputFileName]);

  return (<ExerciseInfo inputFileName={inputFileName} satellite1Data={satellite1Data} satellite2Data={satellite2Data} satellite3Data={satellite3Data} />);
};

export default FetchExerciseInfo;
