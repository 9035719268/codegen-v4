import {render, unmountComponentAtNode} from "react-dom";
import CppTableRow from "./CppTableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../../config/token/token";
import {noOop, withOop} from "../../../../../../../config/label/tablebutton/spring2021/label";
import {cpp} from "../../../../../../../config/label/lang/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<CppTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(withOop);
});

it("shouldRenderCppTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<CppTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<CppTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<CppTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<CppTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe(cpp);
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(noOop);
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe(withOop);
});