import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {SPRING2021_USERS} from "../../../../../../../service/security";
import {withOop} from "../../../../../../../config/label/tablebutton/spring2021/label";
import {spring2021ExerciseInfoFormPath} from "../../../../../../../config/path/front/spring2021/path";
import {java} from "../../../../../../../config/label/lang/label";

const JavaTableRow = () => {
  return (
    <tr>
      <th scope="row">{java}</th>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="java"
          file="true"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
      <td>
        <TableQueryAuthWrapper
          path={spring2021ExerciseInfoFormPath}
          lang="java"
          file="false"
          oop="true"
          text={withOop}
          rolesAllowed={SPRING2021_USERS}
        />
      </td>
    </tr>
  );
};

export default JavaTableRow;