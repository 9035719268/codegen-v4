import {frontEndPort} from "../../../port/port";

export const spring2021ExerciseInfoFormPath = "/year/2021/spring/input-file/select";

export const getSpring2021ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2021/spring/exercise-info/${inputFileName}`;
};

export const getSpring2021ActionOverviewPath = (inputFileName, lang, file, oop) => {
  return `http://localhost:${frontEndPort}/year/2021/spring/actions-overview/${inputFileName}?lang=${lang}&file=${file}&oop=${oop}`;
};