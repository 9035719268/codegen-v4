export const cppMainFileName = "main.cpp";

export const javaMainFileName = "Main.java";

export const javaGraphDrawerFileName = "GraphDrawer.java";

export const pythonMainFileName = "main.py";

export const chartsFileName = "charts.rar";

export const resourcesFileName = "resources.rar";

export const referenceFileName = "reference.pdf";