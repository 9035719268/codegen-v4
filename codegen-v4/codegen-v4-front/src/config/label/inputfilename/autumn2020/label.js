export const autumn2020InputFileNames = [
  "BOGI_1-6.dat",
  "bshm_1-10.dat",
  "HUEG_1-6.dat",
  "hueg_1-10.dat",
  "leij_1-10.dat",
  "ONSA_1-6.dat",
  "onsa_1-10.dat",
  "POTS_6hours.dat",
  "spt0_1-10.dat",
  "svtl_1-10.dat",
  "tit2_1-10.dat",
  "TITZ_6hours.dat",
  "vis0_1-10.dat",
  "warn_1-10.dat",
  "WARN_6hours.dat",
  "zeck_1-10.dat"
];