import {logout} from "../service/security";

const Logout = props => {
  logout();
  props.history.push("/login");

  return <></>
};

export default Logout;