import getValuesDividedByComma from "./getValuesDividedByComma";

it("shouldDivideOneDigitValuesByComma", () => {
  const expectedValues = ["1, ", "2, ", "3, ", "4"];

  const actualValues = getValuesDividedByComma(["1", "2", "3", "4"]);

  expect(actualValues).toStrictEqual(expectedValues);
});

it("shouldDivideMultipleDigitValuesByComma", () => {
  const expectedValues = ["1, ", "23, ", "456, ", "7890, ", "12345, ", "67890"];

  const actualValues = getValuesDividedByComma(["1", "23", "456", "7890", "12345", "67890"]);

  expect(actualValues).toStrictEqual(expectedValues);
});
