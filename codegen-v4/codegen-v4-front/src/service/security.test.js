import {
  ALL_USERS,
  AUTUMN2020_USERS,
  AUTUMN2020_VAR1_USERS, AUTUMN2020_VAR2_USERS, AUTUMN2020_VAR3_USERS,
  checkUserAccess,
  getCurrentUser,
  getCurrentUserRoles, getDecodedJWT, isTokenExpired,
  logout,
  SPRING2021_USERS
} from "./security";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, adminRefreshToken, s2021AccessToken} from "../config/token/token";

it("shouldLogout", () => {
  localStorage.setItem("access_token", adminAccessToken);
  localStorage.setItem("refresh_token", adminRefreshToken);

  logout();

  expect(localStorage.getItem("access_token")).toBe("null");
  expect(localStorage.getItem("refresh_token")).toBe("null");
});

it("shouldCheckUserWithoutTokenAccess", () => {
  const userAccess = checkUserAccess(ALL_USERS);

  expect(userAccess).toBe(true);
});

it("shouldCheckAdminAccess", () => {
  localStorage.setItem("access_token", adminAccessToken);

  const canAccessAdminResource = checkUserAccess(["ROLE_ADMIN"]);
  const canAccessA20201Resource = checkUserAccess(["ROLE_A20201"]);
  const canAccessA20202Resource = checkUserAccess(["ROLE_A20202"]);
  const canAccessA20203Resource = checkUserAccess(["ROLE_A20203"]);
  const canAccessS2021Resource = checkUserAccess(["ROLE_S2021"]);

  expect(canAccessAdminResource).toBe(true);
  expect(canAccessA20201Resource).toBe(false);
  expect(canAccessA20202Resource).toBe(false);
  expect(canAccessA20203Resource).toBe(false);
  expect(canAccessS2021Resource).toBe(false);
});

it("shouldCheckA20201Access", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  const canAccessAdminResource = checkUserAccess(["ROLE_ADMIN"]);
  const canAccessA20201Resource = checkUserAccess(["ROLE_A20201"]);
  const canAccessA20202Resource = checkUserAccess(["ROLE_A20202"]);
  const canAccessA20203Resource = checkUserAccess(["ROLE_A20203"]);
  const canAccessS2021Resource = checkUserAccess(["ROLE_S2021"]);

  expect(canAccessAdminResource).toBe(false);
  expect(canAccessA20201Resource).toBe(true);
  expect(canAccessA20202Resource).toBe(false);
  expect(canAccessA20203Resource).toBe(false);
  expect(canAccessS2021Resource).toBe(false);
});

it("shouldCheckA20202Access", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  const canAccessAdminResource = checkUserAccess(["ROLE_ADMIN"]);
  const canAccessA20201Resource = checkUserAccess(["ROLE_A20201"]);
  const canAccessA20202Resource = checkUserAccess(["ROLE_A20202"]);
  const canAccessA20203Resource = checkUserAccess(["ROLE_A20203"]);
  const canAccessS2021Resource = checkUserAccess(["ROLE_S2021"]);

  expect(canAccessAdminResource).toBe(false);
  expect(canAccessA20201Resource).toBe(false);
  expect(canAccessA20202Resource).toBe(true);
  expect(canAccessA20203Resource).toBe(false);
  expect(canAccessS2021Resource).toBe(false);
});

it("shouldCheckA20203Access", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  const canAccessAdminResource = checkUserAccess(["ROLE_ADMIN"]);
  const canAccessA20201Resource = checkUserAccess(["ROLE_A20201"]);
  const canAccessA20202Resource = checkUserAccess(["ROLE_A20202"]);
  const canAccessA20203Resource = checkUserAccess(["ROLE_A20203"]);
  const canAccessS2021Resource = checkUserAccess(["ROLE_S2021"]);

  expect(canAccessAdminResource).toBe(false);
  expect(canAccessA20201Resource).toBe(false);
  expect(canAccessA20202Resource).toBe(false);
  expect(canAccessA20203Resource).toBe(true);
  expect(canAccessS2021Resource).toBe(false);
});

it("shouldCheckS2021Access", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  const canAccessAdminResource = checkUserAccess(["ROLE_ADMIN"]);
  const canAccessA20201Resource = checkUserAccess(["ROLE_A20201"]);
  const canAccessA20202Resource = checkUserAccess(["ROLE_A20202"]);
  const canAccessA20203Resource = checkUserAccess(["ROLE_A20203"]);
  const canAccessS2021Resource = checkUserAccess(["ROLE_S2021"]);

  expect(canAccessAdminResource).toBe(false);
  expect(canAccessA20201Resource).toBe(false);
  expect(canAccessA20202Resource).toBe(false);
  expect(canAccessA20203Resource).toBe(false);
  expect(canAccessS2021Resource).toBe(true);
});

it("shouldGetCurrentUserAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  const currentUser = getCurrentUser();

  expect(currentUser).toBe("admin");
});

it("shouldGetCurrentUserA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  const currentUser = getCurrentUser();

  expect(currentUser).toBe("a20201");
});

it("shouldGetCurrentUserA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  const currentUser = getCurrentUser();

  expect(currentUser).toBe("a20202");
});

it("shouldGetCurrentUserA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  const currentUser = getCurrentUser();

  expect(currentUser).toBe("a20203");
});

it("shouldGetCurrentUserS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  const currentUser = getCurrentUser();

  expect(currentUser).toBe("s2021");
});

it("shouldGetCurrentUserRolesAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  const currentUserRoles = getCurrentUserRoles();

  expect(currentUserRoles).toStrictEqual(["ROLE_ADMIN"]);
});

it("shouldGetCurrentUserRolesA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  const currentUserRoles = getCurrentUserRoles();

  expect(currentUserRoles).toStrictEqual(["ROLE_A20201"]);
});

it("shouldGetCurrentUserRolesA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  const currentUserRoles = getCurrentUserRoles();

  expect(currentUserRoles).toStrictEqual(["ROLE_A20202"]);
});

it("shouldGetCurrentUserRolesA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  const currentUserRoles = getCurrentUserRoles();

  expect(currentUserRoles).toStrictEqual(["ROLE_A20203"]);
});

it("shouldGetCurrentUserRolesS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  const currentUserRoles = getCurrentUserRoles();

  expect(currentUserRoles).toStrictEqual(["ROLE_S2021"]);
});

it("shouldCheckIfTokenExpiredAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  const tokenExpired = isTokenExpired();

  expect(tokenExpired).toBe(true);
});

it("shouldCheckIfTokenExpiredA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  const tokenExpired = isTokenExpired();

  expect(tokenExpired).toBe(true);
});

it("shouldCheckIfTokenExpiredA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  const tokenExpired = isTokenExpired();

  expect(tokenExpired).toBe(true);
});

it("shouldCheckIfTokenExpiredA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  const tokenExpired = isTokenExpired();

  expect(tokenExpired).toBe(true);
});

it("shouldCheckIfTokenExpiredS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  const tokenExpired = isTokenExpired();

  expect(tokenExpired).toBe(true);
});

it("shouldGetDecodedJWTAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  const decodedJWT = getDecodedJWT();
  const subject = decodedJWT.sub;
  const roles = decodedJWT.roles;

  expect(subject).toBe("admin");
  expect(roles).toStrictEqual(["ROLE_ADMIN"]);
});

it("shouldGetDecodedJWTA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  const decodedJWT = getDecodedJWT();
  const subject = decodedJWT.sub;
  const roles = decodedJWT.roles;

  expect(subject).toBe("a20201");
  expect(roles).toStrictEqual(["ROLE_A20201"]);
});

it("shouldGetDecodedJWTA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  const decodedJWT = getDecodedJWT();
  const subject = decodedJWT.sub;
  const roles = decodedJWT.roles;

  expect(subject).toBe("a20202");
  expect(roles).toStrictEqual(["ROLE_A20202"]);
});

it("shouldGetDecodedJWTA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  const decodedJWT = getDecodedJWT();
  const subject = decodedJWT.sub;
  const roles = decodedJWT.roles;

  expect(subject).toBe("a20203");
  expect(roles).toStrictEqual(["ROLE_A20203"]);
});

it("shouldGetDecodedJWTS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  const decodedJWT = getDecodedJWT();
  const subject = decodedJWT.sub;
  const roles = decodedJWT.roles;

  expect(subject).toBe("s2021");
  expect(roles).toStrictEqual(["ROLE_S2021"]);
});

it("shouldGetAllUsers", () => {
  expect(ALL_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_A20201", "ROLE_A20202", "ROLE_A20203", "ROLE_S2021"]);
});

it("shouldGetAutumn2020Users", () => {
  expect(AUTUMN2020_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_A20201", "ROLE_A20202", "ROLE_A20203"]);
});

it("shouldGetAutumn2020Var1Users", () => {
  expect(AUTUMN2020_VAR1_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_A20201"]);
});

it("shouldGetAutumn2020Var2Users", () => {
  expect(AUTUMN2020_VAR2_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_A20202"]);
});

it("shouldGetAutumn2020Var3Users", () => {
  expect(AUTUMN2020_VAR3_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_A20203"]);
});

it("shouldGetSpring2021Users", () => {
  expect(SPRING2021_USERS).toStrictEqual(["ROLE_ADMIN", "ROLE_S2021"]);
});