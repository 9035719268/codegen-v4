import axios from "axios";
import {getSpring2021ExerciseInfoPath} from "../../../config/path/back/spring2021/task/path";
import {getConfigWithAuthorizationHeader} from "../../security";

const getSatellitesData = inputFileName => {
  const path = getSpring2021ExerciseInfoPath(inputFileName);
  const config = getConfigWithAuthorizationHeader();

  return axios.get(path, config).then(response => {
    const satellites = response.data.satellites;
    const satellite1 = satellites[0];
    const satellite2 = satellites[1];
    const satellite3 = satellites[2];

    return {satellite1, satellite2, satellite3};
  });
};

export default getSatellitesData;