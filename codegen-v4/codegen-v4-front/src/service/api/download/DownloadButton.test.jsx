import {render, unmountComponentAtNode} from "react-dom";
import DownloadButton from "./DownloadButton";
import {downloadMainCpp} from "../../../config/label/overviewbutton/label";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadButton", () => {
  render(<DownloadButton label={downloadMainCpp} />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe(downloadMainCpp);
});