import {downloadFile} from "../fileDownloader";

const DownloadButton = props => {
  const path = props.path;
  const fileName = props.fileName;
  const label = props.label;

  return (
    <button className="overview" type="submit" onClick={() => downloadFile(path, fileName)}>
      {label}
    </button>
  );
};

export default DownloadButton;