import {getRawAccessToken} from "../security";
import axios from "axios";

export const downloadFile = (url, fileName) => {
  const config = getConfigWithAuthorizationHeader();

  axios.get(url, config)
    .then(response => downloadBlob(response, fileName));
};

const getConfigWithAuthorizationHeader = () => {
  const accessToken = getRawAccessToken();

  return {
    responseType: "blob",
    headers: {
      Authorization: accessToken
    }
  };
};

const downloadBlob = (response, fileName) => {
  const fileDownload = require("js-file-download");
  fileDownload(response.data, fileName);
};
