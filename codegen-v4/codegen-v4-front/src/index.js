import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App.jsx";

const Root = (
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

ReactDOM.render(Root, document.getElementById("root"));
