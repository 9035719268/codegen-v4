package com.gvozdev.autumn2020service.util.converter;

import com.gvozdev.autumn2020service.util.queryparam.file.File;
import com.gvozdev.autumn2020service.util.queryparam.file.UrlFileQueryParamConverter;
import org.junit.jupiter.api.Test;

import static com.gvozdev.autumn2020service.util.queryparam.file.File.FILE;
import static com.gvozdev.autumn2020service.util.queryparam.file.File.MANUAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlFileQueryParamConverterTests {
    private final UrlFileQueryParamConverter converter = new UrlFileQueryParamConverter();

    @Test
    void shouldCheckFileValueConversion() {
        File file = converter.convert("file");

        assertEquals(FILE, file);
    }

    @Test
    void shouldCheckManualValueConversion() {
        File manual = converter.convert("manual");

        assertEquals(MANUAL, manual);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> converter.convert("wrong_file_value")
        );
    }
}
