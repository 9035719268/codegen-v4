package com.gvozdev.autumn2020service.util.converter;

import com.gvozdev.autumn2020service.util.queryparam.var.UrlVarQueryParamConverter;
import com.gvozdev.autumn2020service.util.queryparam.var.Var;
import org.junit.jupiter.api.Test;

import static com.gvozdev.autumn2020service.util.queryparam.var.Var.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlVarQueryParamConverterTests {
    private final UrlVarQueryParamConverter converter = new UrlVarQueryParamConverter();

    @Test
    void shouldCheckVar1ValueConversion() {
        Var var1 = converter.convert("var1");

        assertEquals(VAR1, var1);
    }

    @Test
    void shouldCheckVar2ValueConversion() {
        Var var2 = converter.convert("var2");

        assertEquals(VAR2, var2);
    }

    @Test
    void shouldCheckVar3ValueConversion() {
        Var var3 = converter.convert("var3");

        assertEquals(VAR3, var3);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            IllegalArgumentException.class,
            () -> converter.convert("wrong_var_value")
        );
    }
}
