package com.gvozdev.autumn2020service.domain.var3;

import java.util.List;

public record IonCoefficients(List<Double> ionCoefficients) {
}
