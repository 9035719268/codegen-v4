package com.gvozdev.autumn2020service.util.var3.interpolation;

public record InterpolationCoordinates(
    String smallerLatitude, String biggerLatitude,
    String smallerLongitude, String biggerLongitude
) {
}
