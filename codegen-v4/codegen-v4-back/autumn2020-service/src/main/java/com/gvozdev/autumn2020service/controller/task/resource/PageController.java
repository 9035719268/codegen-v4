package com.gvozdev.autumn2020service.controller.task.resource;

import com.gvozdev.autumn2020service.service.resolver.resource.Resolver;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.REFERENCE;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.RESOURCES;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("ResourceController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2020/autumn/download")
public class PageController {
    private final Resolver resolver;

    public PageController(Resolver resolver) {
        this.resolver = resolver;
    }

    @GetMapping("/resources")
    public ResponseEntity<byte[]> downloadResources() {
        byte[] resources = resolver.getResources();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(RESOURCES.getFileName()).build());

        return new ResponseEntity<>(resources, responseHeaders, OK);
    }

    @GetMapping("/reference")
    public ResponseEntity<byte[]> downloadReference() {
        byte[] reference = resolver.getReference();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(REFERENCE.getFileName()).build());

        return new ResponseEntity<>(reference, responseHeaders, OK);
    }
}
