package com.gvozdev.autumn2020service.domain.var3;

import com.gvozdev.autumn2020service.util.var3.interpolation.InterpolationCoordinates;

import java.util.List;

public record Components(
    InterpolationCoordinates interpolationCoordinates,
    GpsTime gpsTime, IonCoefficients alpha, IonCoefficients beta,
    List<Tec> forecastValues, List<Tec> preciseValues
) {
}
