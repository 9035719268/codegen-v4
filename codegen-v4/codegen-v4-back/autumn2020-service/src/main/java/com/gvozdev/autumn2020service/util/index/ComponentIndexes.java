package com.gvozdev.autumn2020service.util.index;

public class ComponentIndexes {
    private final int p1Index;
    private final int p2Index;
    private final int elevationIndex;

    public ComponentIndexes(int p1Index, int p2Index, int elevationIndex) {
        this.p1Index = p1Index;
        this.p2Index = p2Index;
        this.elevationIndex = elevationIndex;
    }

    public int getP1Index() {
        return p1Index;
    }

    public int getP2Index() {
        return p2Index;
    }

    public int getElevationIndex() {
        return elevationIndex;
    }
}
