package com.gvozdev.autumn2020service.util.number;

import static java.util.Objects.hash;

public record ThreeDigitNumber(char firstDigit, char secondDigit, char thirdDigit) {
    private static final int SIZE = 3;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        ThreeDigitNumber otherNumber = (ThreeDigitNumber) other;

        return firstDigit == otherNumber.firstDigit() && secondDigit == otherNumber.secondDigit() && thirdDigit == otherNumber.thirdDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, secondDigit, thirdDigit, SIZE);
    }
}
