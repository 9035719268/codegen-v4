package com.gvozdev.autumn2020service.util.var1.controller;

import com.gvozdev.autumn2020service.domain.var1.Satellite;
import com.gvozdev.autumn2020service.util.common.CommonUtil;
import com.gvozdev.autumn2020service.util.index.ComponentIndexes;
import com.gvozdev.autumn2020service.util.queryparam.lang.Lang;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReader;
import com.gvozdev.autumn2020service.util.var1.filereader.FileReaderUtil;
import com.gvozdev.autumn2020service.util.var1.fileservice.FileService;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.gvozdev.autumn2020service.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.autumn2020service.util.queryparam.lang.Lang.JAVA;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

@Component("Var1ControllerUtil")
public record ControllerUtil(ComponentIndexes componentIndexes, CommonUtil commonUtil, FileReaderUtil fileReaderUtil) {
    public String getFilledListingForFile(byte[] inputFileBytes, byte[] outputFileBytes) {
        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).number();
        int satellite2Number = satellites.get(1).number();
        int satellite3Number = satellites.get(2).number();

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number
        );
    }

    public String getFilledListingForManual(byte[] inputFileBytes, byte[] outputFileBytes, Lang lang) {
        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).number();
        int satellite2Number = satellites.get(1).number();
        int satellite3Number = satellites.get(2).number();

        List<Double> satellite1P1 = satellites.get(0).p1();
        List<Double> satellite1P2 = satellites.get(0).p2();
        List<Double> satellite2P1 = satellites.get(1).p1();
        List<Double> satellite2P2 = satellites.get(1).p2();
        List<Double> satellite3P1 = satellites.get(2).p1();
        List<Double> satellite3P2 = satellites.get(2).p2();

        String satellite1P1WithLineBreaks = getMeasurementsWithLineBreaks(satellite1P1, lang);
        String satellite1P2WithLineBreaks = getMeasurementsWithLineBreaks(satellite1P2, lang);
        String satellite2P1WithLineBreaks = getMeasurementsWithLineBreaks(satellite2P1, lang);
        String satellite2P2WithLineBreaks = getMeasurementsWithLineBreaks(satellite2P2, lang);
        String satellite3P1WithLineBreaks = getMeasurementsWithLineBreaks(satellite3P1, lang);
        String satellite3P2WithLineBreaks = getMeasurementsWithLineBreaks(satellite3P2, lang);

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number,
            satellite1P1WithLineBreaks, satellite1P2WithLineBreaks,
            satellite2P1WithLineBreaks, satellite2P2WithLineBreaks,
            satellite3P1WithLineBreaks, satellite3P2WithLineBreaks
        );
    }

    public ByteArrayOutputStream getChartsArchiveInBytes(byte[] pseudoRangesChartInBytes) {
        String pseudoRangesChartName = "pseudoRangesChart.dat";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);

        ZipEntry pseudoRangesEntry = new ZipEntry(pseudoRangesChartName);
        pseudoRangesEntry.setSize(pseudoRangesChartInBytes.length);

        try {
            zipOutputStream.putNextEntry(pseudoRangesEntry);
            zipOutputStream.write(pseudoRangesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream;
    }

    private static String getMeasurementsWithLineBreaks(List<Double> measurements, Lang lang) {
        return range(0, AMOUNT_OF_OBSERVATIONS)
            .mapToObj(observation -> handleLineBreaks(observation, lang, measurements))
            .collect(joining());
    }

    private static String handleLineBreaks(int observation, Lang lang, List<Double> measurements) {
        if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t\t";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t";
        } else {
            return measurements.get(observation) + " ";
        }
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && !lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && !lang.equals(JAVA);
    }
}
