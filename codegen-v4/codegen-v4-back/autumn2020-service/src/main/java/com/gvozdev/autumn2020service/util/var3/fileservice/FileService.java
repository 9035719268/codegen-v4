package com.gvozdev.autumn2020service.util.var3.fileservice;

import com.gvozdev.autumn2020service.domain.var3.Components;
import com.gvozdev.autumn2020service.domain.var3.GpsTime;
import com.gvozdev.autumn2020service.domain.var3.IonCoefficients;
import com.gvozdev.autumn2020service.domain.var3.Tec;
import com.gvozdev.autumn2020service.util.number.OneDigitNumber;
import com.gvozdev.autumn2020service.util.number.ThreeDigitNumber;
import com.gvozdev.autumn2020service.util.number.TwoDigitNumber;
import com.gvozdev.autumn2020service.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.autumn2020service.util.var3.filereader.IonoFileReader;
import com.gvozdev.autumn2020service.util.var3.interpolation.InterpolationCoordinates;

import java.util.List;

import static java.util.Arrays.asList;

public record FileService(
    EphemerisFileReader ephemerisFileReader,
    IonoFileReader forecastIonoFileReader, IonoFileReader preciseIonoFileReader,
    String satelliteNumber, int forecastTecListFirstLine, int preciseTecListFirstLine
) {
    public Components getComponents(String smallerLatitude, String biggerLatitude, String smallerLongitude, String biggerLongitude) {
        InterpolationCoordinates interpolationCoordinates = new InterpolationCoordinates(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);

        List<Double> gpsTimeValues = extractGpsTime();
        GpsTime gpsTime = createGpsTime(gpsTimeValues);

        List<List<Double>> ionCoefficients = extractIonCoefficients();

        List<Double> alphaCoefficients = ionCoefficients.get(0);
        IonCoefficients alpha = createCoefficients(alphaCoefficients);

        List<Double> betaCoefficients = ionCoefficients.get(1);
        IonCoefficients beta = createCoefficients(betaCoefficients);

        List<List<Integer>> a1TecValues = extractTecValues(biggerLatitude, biggerLongitude);
        List<List<Integer>> a2TecValues = extractTecValues(smallerLatitude, biggerLongitude);
        List<List<Integer>> a3TecValues = extractTecValues(smallerLatitude, smallerLongitude);
        List<List<Integer>> a4TecValues = extractTecValues(biggerLatitude, smallerLongitude);

        List<Integer> forecastA1TecValues = a1TecValues.get(0);
        Tec forecastA1 = createTecValues(forecastA1TecValues);

        List<Integer> forecastA2TecValues = a2TecValues.get(0);
        Tec forecastA2 = createTecValues(forecastA2TecValues);

        List<Integer> forecastA3TecValues = a3TecValues.get(0);
        Tec forecastA3 = createTecValues(forecastA3TecValues);

        List<Integer> forecastA4TecValues = a4TecValues.get(0);
        Tec forecastA4 = createTecValues(forecastA4TecValues);

        List<Integer> preciseA1TecValues = a1TecValues.get(1);
        Tec preciseA1 = createTecValues(preciseA1TecValues);

        List<Integer> preciseA2TecValues = a2TecValues.get(1);
        Tec preciseA2 = createTecValues(preciseA2TecValues);

        List<Integer> preciseA3TecValues = a3TecValues.get(1);
        Tec preciseA3 = createTecValues(preciseA3TecValues);

        List<Integer> preciseA4TecValues = a4TecValues.get(1);
        Tec preciseA4 = createTecValues(preciseA4TecValues);

        return new Components(
            interpolationCoordinates, gpsTime, alpha, beta,
            asList(forecastA1, forecastA2, forecastA3, forecastA4),
            asList(preciseA1, preciseA2, preciseA3, preciseA4)
        );
    }

    private static GpsTime createGpsTime(List<Double> gpsTimeValues) {
        return new GpsTime(gpsTimeValues);
    }

    private static IonCoefficients createCoefficients(List<Double> coefficients) {
        return new IonCoefficients(coefficients);
    }

    private static Tec createTecValues(List<Integer> tecValues) {
        return new Tec(tecValues);
    }

    private List<Double> extractGpsTime() {
        char receivedFirstDigit = satelliteNumber.charAt(0);

        if (satelliteNumber.length() > 1) {
            char receivedSecondDigit = satelliteNumber.charAt(1);

            TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(receivedFirstDigit, receivedSecondDigit);

            return ephemerisFileReader.getGpsTime(requiredSatelliteNumber);
        } else {
            OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(receivedFirstDigit);

            return ephemerisFileReader.getGpsTime(requiredSatelliteNumber);
        }
    }

    private List<List<Double>> extractIonCoefficients() {
        List<Double> alphaCoefficients = ephemerisFileReader.getAlphaCoefficients();
        List<Double> betaCoefficients = ephemerisFileReader.getBetaCoefficients();

        return asList(alphaCoefficients, betaCoefficients);
    }

    private List<List<Integer>> extractTecValues(String latitude, String longitude) {
        char requiredLatitudeFirstDigit = latitude.charAt(0);
        char requiredLatitudeSecondDigit = latitude.charAt(1);
        char requiredLatitudeThirdDigit = latitude.charAt(2);

        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber(requiredLatitudeFirstDigit, requiredLatitudeSecondDigit, requiredLatitudeThirdDigit);

        List<Integer> forecastTecList = forecastIonoFileReader.getTec(requiredLatitudeNumber, longitude, forecastTecListFirstLine);
        List<Integer> preciseTecList = preciseIonoFileReader.getTec(requiredLatitudeNumber, longitude, preciseTecListFirstLine);

        return asList(forecastTecList, preciseTecList);
    }
}
