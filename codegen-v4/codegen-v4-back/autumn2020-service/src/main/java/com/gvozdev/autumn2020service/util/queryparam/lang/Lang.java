package com.gvozdev.autumn2020service.util.queryparam.lang;

public enum Lang {
    CPP, JAVA, PYTHON;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
