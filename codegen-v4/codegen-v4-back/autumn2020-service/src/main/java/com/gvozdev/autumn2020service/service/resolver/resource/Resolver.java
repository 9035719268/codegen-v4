package com.gvozdev.autumn2020service.service.resolver.resource;

import com.gvozdev.autumn2020service.entity.InputFile;
import com.gvozdev.autumn2020service.service.jpa.api.InputFileService;
import com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName;
import org.springframework.stereotype.Component;

import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.REFERENCE;
import static com.gvozdev.autumn2020service.util.queryparam.inputfilename.InputFileName.RESOURCES;

@Component("ResourceResolver")
public record Resolver(InputFileService inputFileService) {
    public byte[] getResources() {
        return getResourceData(RESOURCES);
    }

    public byte[] getReference() {
        return getResourceData(REFERENCE);
    }

    private byte[] getResourceData(InputFileName inputFileName) {
        String name = inputFileName.getFileName();
        InputFile inputFile = inputFileService.findByName(name);

        return inputFile.getFileBytes();
    }
}
