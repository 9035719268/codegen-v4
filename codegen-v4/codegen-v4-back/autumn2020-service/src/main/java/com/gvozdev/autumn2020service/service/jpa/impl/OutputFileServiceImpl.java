package com.gvozdev.autumn2020service.service.jpa.impl;

import com.gvozdev.autumn2020service.entity.OutputFile;
import com.gvozdev.autumn2020service.repository.OutputFileRepository;
import com.gvozdev.autumn2020service.service.jpa.api.OutputFileService;
import org.springframework.stereotype.Service;

@Service
public class OutputFileServiceImpl implements OutputFileService {
    private final OutputFileRepository outputFileRepository;

    public OutputFileServiceImpl(OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    public OutputFile findByParameters(String var, String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(var, lang, file, oop, name);
    }

    @Override
    public void save(OutputFile outputFile) {
        outputFileRepository.save(outputFile);
    }
}
