package com.gvozdev.spring2021service.resolver;

import com.gvozdev.spring2021service.service.resolver.ExerciseInfoWrapper;
import com.gvozdev.spring2021service.service.resolver.Resolver;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.spring2021service.util.queryparam.file.File.FILE;
import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.ARTI_6;
import static com.gvozdev.spring2021service.util.queryparam.lang.Lang.CPP;
import static com.gvozdev.spring2021service.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName.CPP_MAIN;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ResolverTests {
    private final Resolver resolver;

    @Autowired
    ResolverTests(Resolver resolver) {
        this.resolver = resolver;
    }

    @Test
    void shouldCheckExerciseInfoDataCanBeResolved() {
        ExerciseInfoWrapper exerciseInfo = resolver.getExerciseInfo(ARTI_6);

        assertNotNull(exerciseInfo);
    }

    @Test
    void shouldCheckDownloadListingDataCanBeResolved() {
        byte[] listing = resolver.getListing(CPP, FILE, WITHOOP, ARTI_6, CPP_MAIN);

        assertNotNull(listing);
    }

    @Test
    void shouldCheckDownloadChartsDataCanBeResolved() {
        byte[] charts = resolver.getCharts(ARTI_6);

        assertNotNull(charts);
    }

    @Test
    void shouldCheckDownloadResourcesDataCanBeResolved() {
        byte[] resources = resolver.getResources();

        assertNotNull(resources);
    }
}
