package com.gvozdev.spring2021service.util.converter;

import com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.spring2021service.util.queryparam.inputfilename.UrlInputFileNameQueryParamConverter;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UrlInputFileNameQueryParamConverterTests {
    private final UrlInputFileNameQueryParamConverter converter = new UrlInputFileNameQueryParamConverter();

    @Test
    void shouldCheckArti6ValueConversion() {
        InputFileName arti6 = converter.convert("arti_6hours.dat");

        assertEquals(ARTI_6, arti6);
    }

    @Test
    void shouldCheckBshm10ValueConversion() {
        InputFileName bshm10 = converter.convert("bshm_1-10.dat");

        assertEquals(BSHM_10, bshm10);
    }

    @Test
    void shouldCheckGele6ValueConversion() {
        InputFileName gele6 = converter.convert("gele_6hours.dat");

        assertEquals(GELE_6, gele6);
    }

    @Test
    void shouldCheckHueg10ValueConversion() {
        InputFileName hueg10 = converter.convert("hueg_1-10.dat");

        assertEquals(HUEG_10, hueg10);
    }

    @Test
    void shouldCheckIrkm6ValueConversion() {
        InputFileName irkm6 = converter.convert("irkm_6hours.dat");

        assertEquals(IRKM_6, irkm6);
    }

    @Test
    void shouldCheckKslv6ValueConversion() {
        InputFileName kslv6 = converter.convert("kslv_6hours.dat");

        assertEquals(KSLV_6, kslv6);
    }

    @Test
    void shouldCheckLeij10ValueConversion() {
        InputFileName leij10 = converter.convert("leij_1-10.dat");

        assertEquals(LEIJ_10, leij10);
    }

    @Test
    void shouldCheckMaga6ValueConversion() {
        InputFileName maga6 = converter.convert("maga_6hours.dat");

        assertEquals(MAGA_6, maga6);
    }

    @Test
    void shouldCheckMenp6ValueConversion() {
        InputFileName menp6 = converter.convert("menp_6hours.dat");

        assertEquals(MENP_6, menp6);
    }

    @Test
    void shouldCheckNovs6ValueConversion() {
        InputFileName novs6 = converter.convert("novs_6hours.dat");

        assertEquals(NOVS_6, novs6);
    }

    @Test
    void shouldCheckNoyb6ValueConversion() {
        InputFileName noyb6 = converter.convert("noyb_6hours.dat");

        assertEquals(NOYB_6, noyb6);
    }

    @Test
    void shouldCheckOnsa10ValueConversion() {
        InputFileName onsa10 = converter.convert("onsa_1-10.dat");

        assertEquals(ONSA_10, onsa10);
    }

    @Test
    void shouldCheckSpt10ValueConversion() {
        InputFileName spt10 = converter.convert("spt0_1-10.dat");

        assertEquals(SPT_10, spt10);
    }

    @Test
    void shouldCheckSvet6ValueConversion() {
        InputFileName svet6 = converter.convert("svet_6hours.dat");

        assertEquals(SVET_6, svet6);
    }

    @Test
    void shouldCheckSvtl10ValueConversion() {
        InputFileName svtl10 = converter.convert("svtl_1-10.dat");

        assertEquals(SVTL_10, svtl10);
    }

    @Test
    void shouldCheckTit10ValueConversion() {
        InputFileName tit10 = converter.convert("tit2_1-10.dat");

        assertEquals(TIT_10, tit10);
    }

    @Test
    void shouldCheckVis10ValueConversion() {
        InputFileName vis10 = converter.convert("vis0_1-10.dat");

        assertEquals(VIS_10, vis10);
    }

    @Test
    void shouldCheckVlad6ValueConversion() {
        InputFileName vlad6 = converter.convert("vlad_6hours.dat");

        assertEquals(VLAD_6, vlad6);
    }

    @Test
    void shouldCheckWarn10ValueConversion() {
        InputFileName warn10 = converter.convert("warn_1-10.dat");

        assertEquals(WARN_10, warn10);
    }

    @Test
    void shouldCheckYkts6ValueConversion() {
        InputFileName ykts6 = converter.convert("ykts_6hours.dat");

        assertEquals(YKTS_6, ykts6);
    }

    @Test
    void shouldCheckYusa6ValueConversion() {
        InputFileName yusa6 = converter.convert("yusa_6hours.dat");

        assertEquals(YUSA_6, yusa6);
    }

    @Test
    void shouldCheckZeck10ValueConversion() {
        InputFileName zeck10 = converter.convert("zeck_1-10.dat");

        assertEquals(ZECK_10, zeck10);
    }

    @Test
    void shouldCheckResourcesValueConversion() {
        InputFileName resources = converter.convert("resources.rar");

        assertEquals(RESOURCES, resources);
    }

    @Test
    void shouldCheckWrongValueThrowsException() {
        assertThrows(
            NoSuchElementException.class,
            () -> converter.convert("wrong_input_file_value")
        );
    }
}
