package com.gvozdev.spring2021service.util.template.thirdparty;

public class Template {
    public static final String ACTUATOR = """
        /api/year/2021/spring/actuator""";

    public static final String SWAGGER_UI = """
        /api/year/2021/spring/swagger-ui""";

    private Template() {
    }
}
