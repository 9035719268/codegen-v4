package com.gvozdev.spring2021service.util.template.task;

public class Template {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2021/spring/exercise-info/{0}""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2021/spring/download/listing/{0}/{1}/{2}/{3}/{4}""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2021/spring/download/charts/{0}""";

    public static final String DOWNLOAD_RESOURCES = """
    /api/tasks/year/2021/spring/download/resources""";

    private Template() {
    }
}
