package com.gvozdev.spring2021service.file.input;

import com.gvozdev.spring2021service.entity.InputFile;
import com.gvozdev.spring2021service.service.jpa.api.InputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.spring2021service.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FilesExistenceTests {
    private final InputFileService inputFileService;

    @Autowired
    FilesExistenceTests(InputFileService inputFileService) {
        this.inputFileService = inputFileService;
    }

    @Test
    void shouldCheckArti6Exists() {
        InputFile inputFile = inputFileService.findByName(ARTI_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckBshm10Exists() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckGele6Exists() {
        InputFile inputFile = inputFileService.findByName(GELE_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckHueg10Exists() {
        InputFile inputFile = inputFileService.findByName(HUEG_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckIrkm6Exists() {
        InputFile inputFile = inputFileService.findByName(IRKM_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckKslv6Exists() {
        InputFile inputFile = inputFileService.findByName(KSLV_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckLeij10Exists() {
        InputFile inputFile = inputFileService.findByName(LEIJ_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckMaga6Exists() {
        InputFile inputFile = inputFileService.findByName(MAGA_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckMenp6Exists() {
        InputFile inputFile = inputFileService.findByName(MENP_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckNovs6Exists() {
        InputFile inputFile = inputFileService.findByName(NOVS_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckNoyb6Exists() {
        InputFile inputFile = inputFileService.findByName(NOYB_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckOnsa10Exists() {
        InputFile inputFile = inputFileService.findByName(ONSA_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckSpt10Exists() {
        InputFile inputFile = inputFileService.findByName(SPT_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckSvet6Exists() {
        InputFile inputFile = inputFileService.findByName(SVET_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckSvtl10Exists() {
        InputFile inputFile = inputFileService.findByName(SVTL_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckTit10Exists() {
        InputFile inputFile = inputFileService.findByName(TIT_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckVis10Exists() {
        InputFile inputFile = inputFileService.findByName(VIS_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckVlad6Exists() {
        InputFile inputFile = inputFileService.findByName(VLAD_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckWarn10Exists() {
        InputFile inputFile = inputFileService.findByName(WARN_10.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckYkts6Exists() {
        InputFile inputFile = inputFileService.findByName(YKTS_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckYusa6Exists() {
        InputFile inputFile = inputFileService.findByName(YUSA_6.getFileName());

        assertNotNull(inputFile);
    }

    @Test
    void shouldCheckZeck10Exists() {
        InputFile inputFile = inputFileService.findByName(ZECK_10.getFileName());

        assertNotNull(inputFile);
    }
}
