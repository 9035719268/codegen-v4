package com.gvozdev.spring2021service.controller.thirdparty;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static com.gvozdev.spring2021service.util.template.thirdparty.Template.ACTUATOR;
import static com.gvozdev.spring2021service.util.template.thirdparty.Template.SWAGGER_UI;
import static java.lang.String.valueOf;
import static org.springframework.boot.actuate.endpoint.ApiVersion.V3;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PageControllerTests {
    private final MockMvc mockMvc;

    @Autowired
    PageControllerTests(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
    }

    void shouldCheckActuator() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isOk())
            .andExpect(content().contentType(valueOf(V3.getProducedMimeType())));
    }

    @Test
    void shouldCheckSwaggerUi() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/swagger-ui.html"));
    }
}