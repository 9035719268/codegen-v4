package com.gvozdev.spring2021service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@EnableEurekaClient
@SpringBootApplication
public class Spring2021ServiceApplication {
    public static void main(String[] args) {
        run(Spring2021ServiceApplication.class, args);
    }
}