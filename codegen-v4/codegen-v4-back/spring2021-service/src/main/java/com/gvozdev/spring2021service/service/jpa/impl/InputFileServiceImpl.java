package com.gvozdev.spring2021service.service.jpa.impl;

import com.gvozdev.spring2021service.entity.InputFile;
import com.gvozdev.spring2021service.repository.InputFileRepository;
import com.gvozdev.spring2021service.service.jpa.api.InputFileService;
import org.springframework.stereotype.Service;

@Service
public class InputFileServiceImpl implements InputFileService {
    private final InputFileRepository inputFileRepository;

    public InputFileServiceImpl(InputFileRepository inputFileRepository) {
        this.inputFileRepository = inputFileRepository;
    }

    @Override
    public InputFile findByName(String name) {
        return inputFileRepository.findByName(name);
    }

    @Override
    public void save(InputFile inputFile) {
        inputFileRepository.save(inputFile);
    }
}
