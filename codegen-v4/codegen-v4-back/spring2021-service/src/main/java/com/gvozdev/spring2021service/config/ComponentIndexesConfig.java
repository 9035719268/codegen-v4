package com.gvozdev.spring2021service.config;

import com.gvozdev.spring2021service.util.componentindex.ComponentIndexes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ComponentIndexesConfig {

    @Bean
    public ComponentIndexes componentIndexes() {
        int mdIndex = 5;
        int tdIndex = 6;
        int mwIndex = 7;
        int twIndex = 8;
        int elevationIndex = 14;

        return new ComponentIndexes(mdIndex, tdIndex, mwIndex, twIndex, elevationIndex);
    }
}
