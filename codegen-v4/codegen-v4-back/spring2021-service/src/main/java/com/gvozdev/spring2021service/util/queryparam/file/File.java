package com.gvozdev.spring2021service.util.queryparam.file;

public enum File {
    FILE, MANUAL;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
