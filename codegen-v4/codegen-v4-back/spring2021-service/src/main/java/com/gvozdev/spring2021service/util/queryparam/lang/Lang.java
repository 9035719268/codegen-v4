package com.gvozdev.spring2021service.util.queryparam.lang;

public enum Lang {
    CPP, JAVA, PYTHON;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
