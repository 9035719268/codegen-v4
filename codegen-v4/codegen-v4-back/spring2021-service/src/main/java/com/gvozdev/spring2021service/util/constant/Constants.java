package com.gvozdev.spring2021service.util.constant;

public class Constants {
	public static final int AMOUNT_OF_OBSERVATIONS = 360;

	private Constants() {
	}
}
