package com.gvozdev.spring2021service.service.jpa.api;

import com.gvozdev.spring2021service.entity.OutputFile;

public interface OutputFileService {
    OutputFile findByParameters(String lang, String file, String oop, String name);

    void save(OutputFile outputFile);
}
