package com.gvozdev.spring2021service.util.queryparam.outputfilename;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.spring2021service.util.queryparam.outputfilename.OutputFileName.getFileNameFromString;

@Component
public class UrlOutputFileNameQueryParamConverter implements Converter<String, OutputFileName> {

    @Override
    public OutputFileName convert(String source) {
        return getFileNameFromString(source);
    }
}
