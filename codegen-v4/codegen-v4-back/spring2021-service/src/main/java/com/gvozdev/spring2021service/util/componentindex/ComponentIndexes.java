package com.gvozdev.spring2021service.util.componentindex;

public class ComponentIndexes {
    private final int mdIndex;
    private final int tdIndex;
    private final int mwIndex;
    private final int twIndex;
    private final int elevationIndex;

    public ComponentIndexes(int mdIndex, int tdIndex, int mwIndex, int twIndex, int elevationIndex) {
        this.mdIndex = mdIndex;
        this.tdIndex = tdIndex;
        this.mwIndex = mwIndex;
        this.twIndex = twIndex;
        this.elevationIndex = elevationIndex;
    }

    public int getMdIndex() {
        return mdIndex;
    }

    public int getTdIndex() {
        return tdIndex;
    }

    public int getMwIndex() {
        return mwIndex;
    }

    public int getTwIndex() {
        return twIndex;
    }

    public int getElevationIndex() {
        return elevationIndex;
    }
}
