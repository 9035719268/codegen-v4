package com.gvozdev.securityservice.access.security;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.gvozdev.securityservice.domain.Authority;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

import static com.auth0.jwt.JWT.decode;
import static com.gvozdev.securityservice.domain.Authority.*;
import static com.gvozdev.securityservice.util.auth.AntMatchers.LOGIN;
import static com.gvozdev.securityservice.util.auth.AntMatchers.REFRESH_TOKEN;
import static com.jayway.jsonpath.JsonPath.read;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:test-application.properties")
class AccessTests {
    private final MockMvc mockMvc;
    private final Environment environment;

    @Autowired
    AccessTests(MockMvc mockMvc, Environment environment) {
        this.mockMvc = mockMvc;
        this.environment = environment;
    }

    @Test
    void shouldCheckAccessTokenViaCredentialsAdmin() throws Exception {
        String userName = environment.getProperty("admin.username");
        String password = environment.getProperty("admin.password");

        checkGetAccessTokenProcess(userName, password, ROLE_ADMIN);
    }

    @Test
    void shouldCheckRefreshTokenViaCredentialsAdmin() throws Exception {
        String userName = environment.getProperty("admin.username");
        String password = environment.getProperty("admin.password");

        checkGetRefreshTokenProcess(userName, password, ROLE_ADMIN);
    }

    @Test
    void shouldCheckAccessTokenFromRefreshTokenAdmin() throws Exception {
        String userName = environment.getProperty("admin.username");
        String password = environment.getProperty("admin.password");

        checkGetAccessTokenViaRefreshTokenProcess(userName, password, ROLE_ADMIN);
    }

    @Test
    void shouldCheckAccessTokenViaCredentialsA20201() throws Exception {
        String userName = environment.getProperty("a20201.username");
        String password = environment.getProperty("a20201.password");

        checkGetAccessTokenProcess(userName, password, ROLE_A20201);
    }

    @Test
    void shouldCheckRefreshTokenViaCredentialsA20201() throws Exception {
        String userName = environment.getProperty("a20201.username");
        String password = environment.getProperty("a20201.password");

        checkGetRefreshTokenProcess(userName, password, ROLE_A20201);
    }

    @Test
    void shouldCheckAccessTokenFromRefreshTokenA20201() throws Exception {
        String userName = environment.getProperty("a20201.username");
        String password = environment.getProperty("a20201.password");

        checkGetAccessTokenViaRefreshTokenProcess(userName, password, ROLE_A20201);
    }

    @Test
    void shouldCheckAccessTokenViaCredentialsA20202() throws Exception {
        String userName = environment.getProperty("a20202.username");
        String password = environment.getProperty("a20202.password");

        checkGetAccessTokenProcess(userName, password, ROLE_A20202);
    }

    @Test
    void shouldCheckRefreshTokenViaCredentialsA20202() throws Exception {
        String userName = environment.getProperty("a20202.username");
        String password = environment.getProperty("a20202.password");

        checkGetRefreshTokenProcess(userName, password, ROLE_A20202);
    }

    @Test
    void shouldCheckAccessTokenFromRefreshTokenA20202() throws Exception {
        String userName = environment.getProperty("a20202.username");
        String password = environment.getProperty("a20202.password");

        checkGetAccessTokenViaRefreshTokenProcess(userName, password, ROLE_A20202);
    }

    @Test
    void shouldCheckAccessTokenViaCredentialsA20203() throws Exception {
        String userName = environment.getProperty("a20203.username");
        String password = environment.getProperty("a20203.password");

        checkGetAccessTokenProcess(userName, password, ROLE_A20203);
    }

    @Test
    void shouldCheckRefreshTokenViaCredentialsA20203() throws Exception {
        String userName = environment.getProperty("a20203.username");
        String password = environment.getProperty("a20203.password");

        checkGetRefreshTokenProcess(userName, password, ROLE_A20203);
    }

    @Test
    void shouldCheckAccessTokenFromRefreshTokenA20203() throws Exception {
        String userName = environment.getProperty("a20203.username");
        String password = environment.getProperty("a20203.password");

        checkGetAccessTokenViaRefreshTokenProcess(userName, password, ROLE_A20203);
    }

    @Test
    void shouldCheckAccessTokenViaCredentialsS2021() throws Exception {
        String userName = environment.getProperty("s2021.username");
        String password = environment.getProperty("s2021.password");

        checkGetAccessTokenProcess(userName, password, ROLE_S2021);
    }

    @Test
    void shouldCheckRefreshTokenViaCredentialsS2021() throws Exception {
        String userName = environment.getProperty("s2021.username");
        String password = environment.getProperty("s2021.password");

        checkGetRefreshTokenProcess(userName, password, ROLE_S2021);
    }

    @Test
    void shouldCheckAccessTokenFromRefreshTokenS2021() throws Exception {
        String userName = environment.getProperty("s2021.username");
        String password = environment.getProperty("s2021.password");

        checkGetAccessTokenViaRefreshTokenProcess(userName, password, ROLE_S2021);
    }

    private void checkGetAccessTokenProcess(String userName, String password, Authority authority) throws Exception {
        Map<String, String> tokens = getTokensViaCredentials(userName, password);
        String accessToken = tokens.get("access_token");
        DecodedJWT decodedAccessToken = decode(accessToken);

        String header = decodedAccessToken.getHeader();
        String payload = decodedAccessToken.getPayload();
        String signature = decodedAccessToken.getSignature();
        String subject = decodedAccessToken.getSubject();
        List<String> roles = decodedAccessToken.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedAccessToken.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(userName, subject);
        assertThat(roles).contains(authority.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    private void checkGetRefreshTokenProcess(String userName, String password, Authority authority) throws Exception {
        Map<String, String> tokens = getTokensViaCredentials(userName, password);
        String refreshToken = tokens.get("refresh_token");
        DecodedJWT decodedRefreshToken = decode(refreshToken);

        String header = decodedRefreshToken.getHeader();
        String payload = decodedRefreshToken.getPayload();
        String signature = decodedRefreshToken.getSignature();
        String subject = decodedRefreshToken.getSubject();
        List<String> roles = decodedRefreshToken.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedRefreshToken.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(userName, subject);
        assertThat(roles).contains(authority.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    private void checkGetAccessTokenViaRefreshTokenProcess(String userName, String password, Authority authority) throws Exception {
        Map<String, String> oldTokens = getTokensViaCredentials(userName, password);
        String refreshToken = oldTokens.get("refresh_token");
        Map<String, String> newTokens = getTokensViaRefreshToken(refreshToken);
        String newAccessToken = newTokens.get("access_token");
        DecodedJWT decodedAccessToken = decode(newAccessToken);

        String header = decodedAccessToken.getHeader();
        String payload = decodedAccessToken.getPayload();
        String signature = decodedAccessToken.getSignature();
        String subject = decodedAccessToken.getSubject();
        List<String> roles = decodedAccessToken.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedAccessToken.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(userName, subject);
        assertThat(roles).contains(authority.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    private Map<String, String> getTokensViaCredentials(String userName, String password) throws Exception {
        MultiValueMap<String, String> credentials = new LinkedMultiValueMap<>();
        credentials.add("username", userName);
        credentials.add("password", password);

        MvcResult mvcResult = mockMvc.perform(
            post(LOGIN)
                .params(credentials)
                .header(CONTENT_TYPE, APPLICATION_FORM_URLENCODED)
        ).andReturn();

        String responseBody = mvcResult.getResponse().getContentAsString();
        String accessToken = read(responseBody, "$.access_token");
        String refreshToken = read(responseBody, "$.refresh_token");

        return Map.of(
            "access_token", accessToken,
            "refresh_token", refreshToken
        );
    }

    private Map<String, String> getTokensViaRefreshToken(String refreshToken) throws Exception {
        String refreshTokenWithPrefix = "Bearer " + refreshToken;

        MvcResult mvcResult1 = mockMvc.perform(
            get(REFRESH_TOKEN)
                .header(AUTHORIZATION, refreshTokenWithPrefix)
        ).andReturn();

        String responseBody = mvcResult1.getResponse().getContentAsString();
        String newAccessToken = read(responseBody, "$.access_token");
        String newRefreshToken = read(responseBody, "$.refresh_token");

        return Map.of(
            "access_token", newAccessToken,
            "refresh_token", newRefreshToken
        );
    }
}
