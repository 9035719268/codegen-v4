package com.gvozdev.securityservice.config.mockuser;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;

import static com.gvozdev.securityservice.domain.Authority.*;

public class MockUserFactory {
    private MockUserFactory() {
    }

    public static MockUser createAdminUser() {
        Set<SimpleGrantedAuthority> authorities = Set.of(
            new SimpleGrantedAuthority(ROLE_ADMIN.name())
        );

        return new MockUser(authorities);
    }

    public static MockUser createA20201User() {
        Set<SimpleGrantedAuthority> authorities = Set.of(
            new SimpleGrantedAuthority(ROLE_A20201.name())
        );

        return new MockUser(authorities);
    }

    public static MockUser createA20202User() {
        Set<SimpleGrantedAuthority> authorities = Set.of(
            new SimpleGrantedAuthority(ROLE_A20202.name())
        );

        return new MockUser(authorities);
    }

    public static MockUser createA20203User() {
        Set<SimpleGrantedAuthority> authorities = Set.of(
            new SimpleGrantedAuthority(ROLE_A20203.name())
        );

        return new MockUser(authorities);
    }

    public static MockUser createS2021User() {
        Set<SimpleGrantedAuthority> authorities = Set.of(
            new SimpleGrantedAuthority(ROLE_S2021.name())
        );

        return new MockUser(authorities);
    }
}
