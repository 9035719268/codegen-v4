package com.gvozdev.securityservice.util.path.task.autumn2020.var1;

public class Path {
    public static final String EXERCISE_INFO = """
        /api/tasks/year/2020/autumn/var1/exercise-info/POTS_6hours.dat""";

    public static final String DOWNLOAD_LISTING = """
        /api/tasks/year/2020/autumn/var1/download/listing/cpp/file/withoop/POTS_6hours.dat/main.cpp""";

    public static final String DOWNLOAD_CHARTS = """
        /api/tasks/year/2020/autumn/var1/download/charts/POTS_6hours.dat""";

    private Path() {
    }
}
