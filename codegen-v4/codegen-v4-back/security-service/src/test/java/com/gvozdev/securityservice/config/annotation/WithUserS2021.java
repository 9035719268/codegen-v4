package com.gvozdev.securityservice.config.annotation;

import com.gvozdev.securityservice.config.contextfactory.UserS2021ContextFactory;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@WithSecurityContext(factory = UserS2021ContextFactory.class)
public @interface WithUserS2021 {
}
