package com.gvozdev.securityservice.access.thirdparty.spring2021;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.config.annotation.*;
import com.gvozdev.securityservice.controller.thirdparty.spring2021.PageController;
import com.gvozdev.securityservice.util.ServerPingUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Method;

import static com.gvozdev.securityservice.util.path.thirdparty.spring2021.Path.ACTUATOR;
import static com.gvozdev.securityservice.util.path.thirdparty.spring2021.Path.SWAGGER_UI;
import static java.lang.String.valueOf;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.boot.actuate.endpoint.ApiVersion.V3;
import static org.springframework.http.MediaType.TEXT_HTML;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccessTests {
    private final ServerPingUtil serverPingUtil;
    private final MockMvc mockMvc;

    @Autowired
    AccessTests(ServerPingUtil serverPingUtil, MockMvc mockMvc) {
        this.serverPingUtil = serverPingUtil;
        this.mockMvc = mockMvc;
    }

    @BeforeEach
    void checkIfSpring2021ServiceOnline() {
        assumeTrue(
            serverPingUtil.isSpring2021ServiceOnline(),
            "Сервис Spring2021 недоступен"
        );
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessGetSwaggerPath() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().isOk())
            .andExpect(content().contentType(TEXT_HTML));
    }

    @Test
    @WithUserAdmin
    void shouldCheckAdminAccessGetActuatorPath() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isOk())
            .andExpect(content().contentType(valueOf(V3.getProducedMimeType())));
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessGetSwaggerPath() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20201
    void shouldCheckA20201AccessGetActuatorPath() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessGetSwaggerPath() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20202
    void shouldCheckA20202AccessGetActuatorPath() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessGetSwaggerPath() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserA20203
    void shouldCheckA20203AccessGetActuatorPath() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessGetSwaggerPath() throws Exception {
        mockMvc.perform(get(SWAGGER_UI))
            .andExpect(status().isOk())
            .andExpect(content().contentType(TEXT_HTML));
    }

    @Test
    @WithUserS2021
    void shouldCheckS2021AccessGetActuatorPath() throws Exception {
        mockMvc.perform(get(ACTUATOR))
            .andExpect(status().isOk())
            .andExpect(content().contentType(valueOf(V3.getProducedMimeType())));
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method getActuator = PageController.class.getMethod("getActuator");
        Method getSwagger = PageController.class.getMethod("getSwagger");

        assertTrue(getActuator.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(getSwagger.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
