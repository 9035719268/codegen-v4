package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.annotation.WithUserS2021;
import com.gvozdev.securityservice.config.mockuser.MockUserFactory;
import com.gvozdev.securityservice.config.mockuser.MockUser;

public class UserS2021ContextFactory extends AbstractWithSecurityContextFactory<WithUserS2021> {

    @Override
    public MockUser getMockUser() {
        return MockUserFactory.createS2021User();
    }
}
