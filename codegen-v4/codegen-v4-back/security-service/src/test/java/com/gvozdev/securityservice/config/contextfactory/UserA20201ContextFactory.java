package com.gvozdev.securityservice.config.contextfactory;

import com.gvozdev.securityservice.config.annotation.WithUserA20201;
import com.gvozdev.securityservice.config.mockuser.MockUser;

import static com.gvozdev.securityservice.config.mockuser.MockUserFactory.createA20201User;

public class UserA20201ContextFactory extends AbstractWithSecurityContextFactory<WithUserA20201> {

    @Override
    public MockUser getMockUser() {
        return createA20201User();
    }
}
