package com.gvozdev.securityservice.controller.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.securityservice.service.jdbc.UserService;
import com.gvozdev.securityservice.service.token.api.TokenService;
import com.gvozdev.securityservice.util.auth.AuthUtil;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController("TokenController")
@RequestMapping("/api/token")
public class TokenController {
    private static final String SERVICE_NAME = "SECURITY-SERVICE";

    private final TokenService tokenService;
    private final UserService userService;
    private final AuthUtil authUtil;

    public TokenController(TokenService tokenService, UserService userService, AuthUtil authUtil) {
        this.tokenService = tokenService;
        this.userService = userService;
        this.authUtil = authUtil;
    }

    @GetMapping("/refresh")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "tokenServiceFallback")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (authUtil.isTokenValid(request)) {
            try {
                Map<String, String> tokens = tokenService.getNewTokens(request, userService);
                response.setContentType(APPLICATION_JSON_VALUE);

                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception exception) {
                Map<String, String> errorInfo = tokenService.getRefreshTokenErrorInfo(exception);
                tokenService.handleRefreshTokenError(response);

                new ObjectMapper().writeValue(response.getOutputStream(), errorInfo);
            }
        } else {
            Map<String, String> errorInfo = tokenService.getInvalidRefreshTokenErrorInfo();
            tokenService.handleInvalidRefreshToken(response);

            new ObjectMapper().writeValue(response.getOutputStream(), errorInfo);
        }
    }

    private static ResponseEntity<String> tokenServiceFallback(Exception exception) {
        return new ResponseEntity<>("Security сервис недоступен", SERVICE_UNAVAILABLE);
    }
}
