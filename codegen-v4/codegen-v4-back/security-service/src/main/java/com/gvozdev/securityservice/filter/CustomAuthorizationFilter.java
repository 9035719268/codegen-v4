package com.gvozdev.securityservice.filter;

import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvozdev.securityservice.service.filter.api.AuthorizationFilterService;
import com.gvozdev.securityservice.util.auth.AuthUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

public class CustomAuthorizationFilter extends OncePerRequestFilter {
    private final AuthorizationFilterService authorizationFilterService;
    private final AuthUtil authUtil;
    private Algorithm algorithm;

    public CustomAuthorizationFilter(AuthorizationFilterService authorizationFilterService, AuthUtil authUtil, Algorithm algorithm) {
        this.authorizationFilterService = authorizationFilterService;
        this.authUtil = authUtil;
        this.algorithm = algorithm;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String servletPath = request.getServletPath();

        if (algorithm == null) {
            algorithm = authUtil.getAlgorithm(request);
        }

        SecurityContext context = getContext();
        boolean shouldSetAuthorization = authUtil.shouldSetAuthorization(request, servletPath);

        if (authUtil.authorizationExists(context)) {
            filterChain.doFilter(request, response);
        } else if (shouldSetAuthorization) {
            try {
                Authentication jwtAuthentication = authorizationFilterService.getJwtAuthentication(request, algorithm);

                getContext().setAuthentication(jwtAuthentication);

                filterChain.doFilter(request, response);
            } catch (Exception exception) {
                Map<String, String> errorInfo = authorizationFilterService.getJwtAuthenticationErrorInfo(exception);
                authorizationFilterService.handleJwtAuthenticationError(response);

                new ObjectMapper().writeValue(response.getOutputStream(), errorInfo);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
