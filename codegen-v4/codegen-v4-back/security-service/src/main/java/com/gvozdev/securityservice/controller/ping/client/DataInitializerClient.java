package com.gvozdev.securityservice.controller.ping.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/initializer/actuator", name = "DATA-INITIALIZER-PING-CLIENT")
public interface DataInitializerClient extends BasePingClient {
}
