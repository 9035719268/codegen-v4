package com.gvozdev.securityservice.dao;

import com.gvozdev.securityservice.domain.Student;

import java.util.Optional;

public interface UserDao {
    Optional<Student> findUserByUserName(String userName);
}
