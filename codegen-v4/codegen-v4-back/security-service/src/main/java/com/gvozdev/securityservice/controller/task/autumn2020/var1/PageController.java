package com.gvozdev.securityservice.controller.task.autumn2020.var1;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("Autumn2020Var1Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var1")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private static final String SERVICE_NAME = "AUTUMN2020-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/exercise-info/{inputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<String> getExerciseInfo(@PathVariable("inputFileName") String inputFileName) {
        return client.getExerciseInfo(inputFileName);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("inputFileName") String inputFileName,
        @PathVariable("outputFileName") String outputFileName
    ) {
        return client.downloadListing(lang, file, oop, inputFileName, outputFileName);
    }

    @GetMapping("/download/charts/{inputFileName}")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "autumn2020ServiceFallback")
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") String inputFileName) {
        return client.downloadCharts(inputFileName);
    }

    private static ResponseEntity<String> autumn2020ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Autumn2020 недоступен", SERVICE_UNAVAILABLE);
    }
}