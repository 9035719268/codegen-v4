package com.gvozdev.securityservice.util.auth;

public class SqlRequests {
    public static final String FIND_STUDENT_BY_USER_NAME = """
        SELECT * FROM student WHERE user_name = :userName""";

    public static final String GET_STUDENT_AUTHORITIES = """
        SELECT authority FROM role\040
        WHERE id = (SELECT role_id FROM student_role WHERE student_id = :studentId)""";

    private SqlRequests() {
    }
}

