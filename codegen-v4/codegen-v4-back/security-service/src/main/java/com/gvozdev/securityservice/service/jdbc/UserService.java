package com.gvozdev.securityservice.service.jdbc;

import com.gvozdev.securityservice.domain.Student;

public interface UserService {
    Student getUserByUserName(String userName);
}
