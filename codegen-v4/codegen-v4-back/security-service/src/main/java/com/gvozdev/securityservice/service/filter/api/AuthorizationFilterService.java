package com.gvozdev.securityservice.service.filter.api;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface AuthorizationFilterService {
    Authentication getJwtAuthentication(HttpServletRequest request, Algorithm algorithm);

    void handleJwtAuthenticationError(HttpServletResponse response);

    Map<String, String> getJwtAuthenticationErrorInfo(Exception exception);
}
