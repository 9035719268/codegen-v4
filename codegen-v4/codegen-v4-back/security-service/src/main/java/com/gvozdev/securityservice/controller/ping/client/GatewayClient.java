package com.gvozdev.securityservice.controller.ping.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(url = "http://localhost:${gateway-service.port}/api/actuator", name = "GATEWAY-PING-CLIENT")
public interface GatewayClient extends BasePingClient {
}
