package com.gvozdev.securityservice.controller.thirdparty.spring2021;

import com.gvozdev.securityservice.annotation.LoggableSecureEndpoint;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@RestController("Spring2021ThirdPartyController")
@RequestMapping("/api/year/2021/spring")
public class PageController {
    private static final String SERVICE_NAME = "SPRING2021-SERVICE";

    private final Client client;

    public PageController(Client client) {
        this.client = client;
    }

    @GetMapping("/actuator/health")
    public ResponseEntity<String> getActuator() {
        return client.getActuator();
    }

    @GetMapping("/swagger-ui")
    @LoggableSecureEndpoint
    @CircuitBreaker(name = SERVICE_NAME, fallbackMethod = "spring2021ServiceFallback")
    public ResponseEntity<String> getSwagger() {
        return client.getSwagger();
    }

    private static ResponseEntity<String> spring2021ServiceFallback(Exception exception) {
        return new ResponseEntity<>("Сервис Spring2021 недоступен", SERVICE_UNAVAILABLE);
    }
}
