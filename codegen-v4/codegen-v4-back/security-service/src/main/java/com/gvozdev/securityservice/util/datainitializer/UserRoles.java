package com.gvozdev.securityservice.util.datainitializer;

import static com.gvozdev.securityservice.domain.Authority.ROLE_ADMIN;

public class UserRoles {
    public static final String[] DATA_INITIALIZER_ALL = {
        ROLE_ADMIN.getRole()
    };

    private UserRoles() {
    }
}
