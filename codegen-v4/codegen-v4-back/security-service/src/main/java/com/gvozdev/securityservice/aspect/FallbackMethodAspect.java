package com.gvozdev.securityservice.aspect;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Slf4j
@Component
public class FallbackMethodAspect {

    @Pointcut("@annotation(io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker)")
    public void circuitBreakablePointcut() {
    }

    @AfterThrowing("circuitBreakablePointcut()")
    public void logCircuitBroken(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        CircuitBreaker circuitBreaker = method.getAnnotation(CircuitBreaker.class);
        String serviceName = circuitBreaker.name();

        log.error("Вызвался fallback-метод для сервиса {}", serviceName);
    }
}
