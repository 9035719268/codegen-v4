package com.gvozdev.securityservice.util.spring2021;

import static com.gvozdev.securityservice.domain.Authority.ROLE_ADMIN;
import static com.gvozdev.securityservice.domain.Authority.ROLE_S2021;

public class UserRoles {
    public static final String[] SPRING2021_ALL = {
        ROLE_ADMIN.getRole(),
        ROLE_S2021.getRole()
    };

    private UserRoles() {
    }
}
