package com.gvozdev.securityservice.controller.ping.client;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

public interface BasePingClient {

    @GetMapping("/health")
    ResponseEntity<String> getHealth();
}
