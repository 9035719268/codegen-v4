CREATE TABLE IF NOT EXISTS autumn_2020_input_file
(
    id         BIGINT PRIMARY KEY,
    file_bytes BYTEA   NOT NULL,
    name       VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS autumn_2020_output_file
(
    id            BIGINT PRIMARY KEY,
    var           VARCHAR NOT NULL,
    lang          VARCHAR NOT NULL,
    file          VARCHAR NOT NULL,
    oop           VARCHAR NOT NULL,
    name          VARCHAR NOT NULL,
    file_bytes    BYTEA   NOT NULL,
    creation_date DATE    NOT NULL
);