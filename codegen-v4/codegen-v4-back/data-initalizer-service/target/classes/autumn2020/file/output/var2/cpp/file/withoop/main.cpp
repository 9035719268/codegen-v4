﻿#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

class FileReader {
private:
    std::vector<char> allBytes;
    std::vector<std::vector<std::vector<char>>> allLines;

    double getNumeric(std::vector<std::vector<char>> line, int number) {
        std::string numberBuilder = "";
        int numberLength = line.at(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.at(number).at(digit);
            numberBuilder += symbol;
        }
        double numeric = std::stod(numberBuilder);
        return numeric;
    }

    auto analyzeSyntaxAndReturnLinesList() {
        std::vector<std::vector<std::vector<char>>> allLines;
        auto words = new std::vector<std::vector<char>>;
        auto symbols = new std::vector<char>;
        bool isWord = false;

        char tab = 9;
        char newLine = 10;
        char carriageReturn = 13;
        char space = 32;

        for (char symbol : allBytes) {
            if (symbol == newLine) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                allLines.push_back(*words);
                delete words;
                words = new std::vector<std::vector<char>>;
                isWord = false;
            }
            else if ((isWord) && (symbol == tab)) {
                words->push_back(*symbols);
                delete symbols;
                symbols = new std::vector<char>;
                isWord = false;
            }
            else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols->push_back(symbol);
            }
        }
        return allLines;
    }

public:
    FileReader(std::string fileName) {
        std::ifstream in(fileName);
        char byte;
        if (in.is_open()) {
            while (in.get(byte)) {
                allBytes.push_back(byte);
            }
        }
        else {
            exit(1);
        }
        in.close();
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    auto getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2) {
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        int requiredSatelliteNumberSize = 2;

        for (auto line : allLines) {
            try {
                char satelliteNumber1 = line.at(0).at(0);
                char satelliteNumber2 = line.at(0).at(1);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        return measurements;
    }

    auto getMeasurements(char requiredSatelliteNumber) {
        std::vector<std::vector<double>> measurements;
        int numbersInLine = 21;

        int requiredSatelliteNumberSize = 1;

        for (auto line : allLines) {
            try {
                char satelliteNumber = line.at(0).at(0);
                int satelliteNumberSize = line.at(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    std::vector<double> lineOfNumbers;
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.push_back(numeric);
                    }
                    measurements.push_back(lineOfNumbers);
                }
            }
            catch (std::exception ignored) {}
        }
        return measurements;
    }
};

class ElevationAngles {
public:
    ElevationAngles(std::vector<double> elevationAngles) {
        this->elevationAngles = elevationAngles;
    }

    double getAngleAt(int observation) {
        return elevationAngles.at(observation);
    }

private:
    std::vector<double> elevationAngles;
};

class LinearVelocities {
public:
    LinearVelocities(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    auto getLinearVelocities() {
        double gravitational = 6.67 * pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6371000;
        double flightHeight = 20000;

        for (int observation = 0; observation < amountOfObservations; observation++) {
            linearVelocities.push_back(sqrt(gravitational * earthMass / (earthRadius + flightHeight)));
        }
        return linearVelocities;
    }

    std::vector<double> getAverageLinearVelocities() {
        std::vector<double> averageVelocities;
        std::vector<double> linearVelocities = getLinearVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(linearVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(linearVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(linearVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.push_back(firstHourAverage);
        averageVelocities.push_back(secondHourAverage);
        averageVelocities.push_back(thirdHourAverage);
        return averageVelocities;
    }

private:
    int amountOfObservations;
    std::vector<double> linearVelocities;

    double getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
        double hourSum = 0;
        for (int observation = start; observation < end; observation++) {
            hourSum += velocities.at(observation);
        }
        return hourSum;
    }
};

class AngularVelocities {
public:
    AngularVelocities(ElevationAngles& elevationAngles, int amountOfObservations) {
        this->elevationAngles = &elevationAngles;
        this->amountOfObservations = amountOfObservations;
    }

    auto getAngularVelocities() {
        double oneHourInSeconds = 3600;
        double previousElevationAngle = elevationAngles->getAngleAt(0);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAngles->getAngleAt(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.push_back(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }
        return angularVelocities;
    }

    std::vector<double> getAverageAngularVelocities() {
        std::vector<double> averageVelocities;
        std::vector<double> angularVelocities = getAngularVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.push_back(firstHourAverage);
        averageVelocities.push_back(secondHourAverage);
        averageVelocities.push_back(thirdHourAverage);
        return averageVelocities;
    }

private:
    int amountOfObservations;
    ElevationAngles* elevationAngles;
    std::vector<double> angularVelocities;

    double getVelocitySumPerHour(std::vector<double> velocities, int start, int end) {
        double velocitySum = 0;
        for (int observation = start; observation < end; observation++) {
            velocitySum += velocities.at(observation);
        }
        return velocitySum;
    }
};

class Satellite {
public:
    Satellite(int number, AngularVelocities& angularVelocities, LinearVelocities& linearVelocities) {
        this->number = number;
        this->angularVelocities = &angularVelocities;
        this->linearVelocities = &linearVelocities;
    }

    int getNumber() {
        return number;
    }

    auto getAngularVelocities() {
        auto velocities = angularVelocities->getAngularVelocities();
        return velocities;
    }

    auto getAverageAngularVelocities() {
        auto velocities = angularVelocities->getAverageAngularVelocities();
        return velocities;
    }

    auto getLinearVelocities() {
        auto velocities = linearVelocities->getLinearVelocities();
        return velocities;
    }

    auto getAverageLinearVelocities() {
        auto velocities = linearVelocities->getAverageLinearVelocities();
        return velocities;
    }

private:
    int number;
    AngularVelocities* angularVelocities;
    LinearVelocities* linearVelocities;
};

class SatelliteFactory {
public:
    SatelliteFactory(FileReader& fileReader, int amountOfObservations) {
        this->fileReader = &fileReader;
        this->amountOfObservations = amountOfObservations;
    }

    Satellite* createSatellite(int requiredSatelliteNumber) {
        std::vector<double> elevationAnglesArray;
        if (requiredSatelliteNumber < 10) {
            auto satelliteNumber = std::to_string(requiredSatelliteNumber).at(0);
            elevationAnglesArray = getElevationAnglesArray(satelliteNumber);
        }
        else {
            auto satelliteNumber1 = std::to_string(requiredSatelliteNumber).at(0);
            auto satelliteNumber2 = std::to_string(requiredSatelliteNumber).at(1);
            elevationAnglesArray = getElevationAnglesArray(satelliteNumber1, satelliteNumber2);
        }

        ElevationAngles* elevationAngles = new ElevationAngles(elevationAnglesArray);
        AngularVelocities* angularVelocities = new AngularVelocities(*elevationAngles, amountOfObservations);
        LinearVelocities* linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite* satellite = new Satellite(requiredSatelliteNumber, *angularVelocities, *linearVelocities);
        return satellite;
    }

private:
    int amountOfObservations;
    FileReader* fileReader;

    std::vector<double> getElevationAnglesArray(char satelliteNumber1, char satelliteNumber2) {
        auto measurements = fileReader->getMeasurements(satelliteNumber1, satelliteNumber2);
        std::vector<double> elevationAnglesArray;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.at(observation).at(14);
            elevationAnglesArray.push_back(elevationAngle);
        }
        return elevationAnglesArray;
    }

    std::vector<double> getElevationAnglesArray(char satelliteNumber) {
        auto measurements = fileReader->getMeasurements(satelliteNumber);
        std::vector<double> elevationAnglesArray;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.at(observation).at(14);
            elevationAnglesArray.push_back(elevationAngle);
        }
        return elevationAnglesArray;
    }
};

class TemplateConsoleOutput {
public:
    TemplateConsoleOutput(int amountOfObservations) {
        this->amountOfObservations = amountOfObservations;
    }

    void print() {
        int satellite1Number = getSatellite1Number();
        int satellite2Number = getSatellite2Number();
        int satellite3Number = getSatellite3Number();
        std::cout << "Satellite #" << satellite1Number << "\t\tSatellite #" << satellite2Number <<
            "\t\tSatellite #" << satellite3Number << std::endl;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = getVelocities1().at(observation);
            double satellite2Velocity = getVelocities2().at(observation);
            double satellite3Velocity = getVelocities3().at(observation);
            std::cout << satellite1Velocity << "\t" << satellite2Velocity << "\t" << satellite3Velocity << std::endl;
        }
        std::cout << "***********************************************" << std::endl;
    }

protected:
    virtual int getSatellite1Number() = 0;
    virtual int getSatellite2Number() = 0;
    virtual int getSatellite3Number() = 0;
    virtual std::vector<double> getVelocities1() = 0;
    virtual std::vector<double> getVelocities2() = 0;
    virtual std::vector<double> getVelocities3() = 0;

private:
    int amountOfObservations;
};

class AngularVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AngularVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getVelocities1() override {
        auto velocities = satellite1->getAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities2() override {
        auto velocities = satellite2->getAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities3() override {
        auto velocities = satellite3->getAngularVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class AverageAngularVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AverageAngularVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getVelocities1() override {
        auto velocities = satellite1->getAverageAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities2() override {
        auto velocities = satellite2->getAverageAngularVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities3() override {
        auto velocities = satellite3->getAverageAngularVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class LinearVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    LinearVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getVelocities1() override {
        auto velocities = satellite1->getLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities2() override {
        auto velocities = satellite2->getLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities3() override {
        auto velocities = satellite3->getLinearVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class AverageLinearVelocitiesConsoleOutput : public TemplateConsoleOutput {
public:
    AverageLinearVelocitiesConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations)
        : TemplateConsoleOutput(amountOfObservations) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
    }

    virtual int getSatellite1Number() override {
        int number = satellite1->getNumber();
        return number;
    }

    virtual int getSatellite2Number() override {
        int number = satellite2->getNumber();
        return number;
    }

    virtual int getSatellite3Number() override {
        int number = satellite3->getNumber();
        return number;
    }

    virtual std::vector<double> getVelocities1() override {
        auto velocities = satellite1->getAverageLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities2() override {
        auto velocities = satellite2->getAverageLinearVelocities();
        return velocities;
    }

    virtual std::vector<double> getVelocities3() override {
        auto velocities = satellite3->getAverageLinearVelocities();
        return velocities;
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
};

class ConsoleOutput {
public:
    ConsoleOutput(Satellite& satellite1, Satellite& satellite2, Satellite& satellite3, int amountOfObservations, int amountOfHours) {
        this->satellite1 = &satellite1;
        this->satellite2 = &satellite2;
        this->satellite3 = &satellite3;
        this->amountOfObservations = amountOfObservations;
        this->amountOfHours = amountOfHours;
    }

    void getConsoleOutput() {
        AngularVelocitiesConsoleOutput angularVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
        angularVelocitiesOutput.print();

        AverageAngularVelocitiesConsoleOutput averageAngularVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfHours);
        averageAngularVelocitiesOutput.print();

        LinearVelocitiesConsoleOutput linearVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfObservations);
        linearVelocitiesOutput.print();

        AverageLinearVelocitiesConsoleOutput averageLinearVelocitiesOutput(*satellite1, *satellite2, *satellite3, amountOfHours);
        averageLinearVelocitiesOutput.print();
    }

private:
    Satellite* satellite1;
    Satellite* satellite2;
    Satellite* satellite3;
    int amountOfObservations;
    int amountOfHours;
};

int main() {
    int amountOfObservations = 360;
    int amountOfHours = 3;

    int satellite1Number = %s;
    int satellite2Number = %s;
    int satellite3Number = %s;

    FileReader* fileReader = new FileReader("C:\\Users\\User1\\Desktop\\resources\\file.dat");
    SatelliteFactory* satelliteFactory = new SatelliteFactory(*fileReader, amountOfObservations);

    Satellite* satellite1 = satelliteFactory->createSatellite(satellite1Number);
    Satellite* satellite2 = satelliteFactory->createSatellite(satellite2Number);
    Satellite* satellite3 = satelliteFactory->createSatellite(satellite3Number);

    ConsoleOutput* consoleOutput = new ConsoleOutput(*satellite1, *satellite2, *satellite3, amountOfObservations, amountOfHours);
    consoleOutput->getConsoleOutput();
}