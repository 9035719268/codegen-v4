import math
import matplotlib.pyplot as plt
import numpy as np


class UserGeo:
    def __init__(self, lat: float, lon: float) -> None:
        halfCircle: float = 180
        self.__latpp: float = lat / halfCircle
        self.__lonpp: float = lon / halfCircle

    @property
    def latpp(self) -> float:
        return self.__latpp

    @property
    def lonpp(self) -> float:
        return self.__lonpp


class IgpGeo:
    def __init__(self, lon1, lon2, lat1, lat2):
        halfCircle: float = 180
        self.__lon1: float = lon1 / halfCircle
        self.__lon2: float = lon2 / halfCircle
        self.__lat1: float = lat1 / halfCircle
        self.__lat2: float = lat2 / halfCircle

    @property
    def lon1(self) -> float:
        return self.__lon1

    @property
    def lon2(self) -> float:
        return self.__lon2

    @property
    def lat1(self) -> float:
        return self.__lat1

    @property
    def lat2(self) -> float:
        return self.__lat2


class AxisGeo:
    def __init__(self, userGeo: UserGeo, igpGeo: IgpGeo) -> None:
        lonpp: float = userGeo.lonpp
        latpp: float = userGeo.latpp

        lon1: float = igpGeo.lon1
        lon2: float = igpGeo.lon2
        lat1: float = igpGeo.lat1
        lat2: float = igpGeo.lat2

        self.__xpp: float = (lonpp - lon1) / (lon2 - lon1)
        self.__ypp: float = (latpp - lat1) / (lat2 - lat1)

    @property
    def xpp(self) -> float:
        return self.__xpp

    @property
    def ypp(self) -> float:
        return self.__ypp


class WeightMatrix:
    def __init__(self, axisGeo: AxisGeo) -> None:
        self.__weights: list = [0] * 4
        xpp: float = axisGeo.xpp
        ypp: float = axisGeo.ypp
        self.__weights[0] = xpp * ypp
        self.__weights[1] = (1 - xpp) * ypp
        self.__weights[2] = (1 - xpp) * (1 - ypp)
        self.__weights[3] = xpp * (1 - ypp)

    def __getitem__(self, pos) -> float:
        return self.__weights[pos]


class IonCoefficients:
    def __init__(self, coefficients: list) -> None:
        self.__coefficients: list = coefficients

    def __getitem__(self, pos) -> float:
        return self.__coefficients[pos]


class Tec:
    def __init__(self, tec: list) -> None:
        self.__tec: list = tec

    def __getitem__(self, pos: int) -> float:
        return self.__tec[pos]


class GpsTime:
    def __init__(self, gpsTime: list) -> None:
        self.__gpsTime: list = gpsTime

    def __getitem__(self, pos: int) -> float:
        return self.__gpsTime[pos]


class IonosphericDelay:
    def __init__(self, weightMatrix: WeightMatrix, tec: Tec) -> None:
        self.__weightMatrix: WeightMatrix = weightMatrix
        self.__tec: Tec = tec

    @property
    def delayInMeters(self) -> float:
        tecuToMetersCoefficient: float = self.__tecuToMetersCoefficient
        delayInTecu: float = self.__delayInTecu
        delayInMeters: float = delayInTecu * tecuToMetersCoefficient
        return delayInMeters

    @property
    def __tecuToMetersCoefficient(self) -> float:
        l1: float = 1_575_420_000
        oneTecUnit: float = 1E16
        coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
        return coefficient

    @property
    def __delayInTecu(self) -> float:
        delay: float = 0
        for observation in range(4):
            weight: float = self.__weightMatrix[observation]
            rawTec: float = self.__tec[observation]
            tecInOneTecUnit: float = rawTec * 0.1
            delay += (weight * tecInOneTecUnit)
        return delay


class KlobucharModel:
    def __init__(self, latpp: float, lonpp: float, gpsTime: float, alpha: IonCoefficients, beta: IonCoefficients):
        halfCircle: float = 180
        self.__latpp: float = latpp
        self.__lonpp: float = lonpp
        self.__gpsTime: float = gpsTime
        self.__elevationAngle: float = 90 / halfCircle
        self.__azimuth: float = 0
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta

    @property
    def klobucharDelayInMeters(self) -> float:
        delayInSeconds: float = self.__klobucharDelayInSeconds
        speedOfLight: float = 2.99792458 * 1E8
        delayInMeters: float = delayInSeconds * speedOfLight
        return delayInMeters

    @property
    def __earthCenteredAngle(self) -> float:
        earthCenteredAngle: float = 0.0137 / (self.__elevationAngle + 0.11) - 0.022
        return earthCenteredAngle

    @property
    def __ippLatitude(self) -> float:
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = self.__latpp + earthCenteredAngle * math.cos(self.__azimuth)
        if ippLatitude > 0.416:
            ippLatitude = 0.416
        elif ippLatitude < -0.416:
            ippLatitude = -0.416
        return ippLatitude

    @property
    def __ippLongtitude(self) -> float:
        earthCenteredAngle: float = self.__earthCenteredAngle
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = self.__lonpp + (earthCenteredAngle * math.sin(self.__azimuth) / (math.cos(ippLatitude)))
        return ippLongtitude

    @property
    def __ippGeomagneticLatitude(self) -> float:
        ippLatitude: float = self.__ippLatitude
        ippLongtitude: float = self.__ippLongtitude
        ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
        return ippGeomagneticLatitude

    @property
    def __ippLocalTime(self) -> float:
        secondsInOneDay: float = 86_400
        secondsInTwelveHours: float = 43_200
        ippLongtitude: float = self.__ippLongtitude
        ippLocalTime: float = secondsInTwelveHours * ippLongtitude + self.__gpsTime
        while ippLocalTime > secondsInOneDay:
            ippLocalTime -= secondsInOneDay
        while ippLocalTime < 0:
            ippLocalTime += secondsInOneDay
        return ippLocalTime

    @property
    def __ionosphericDelayAmplitude(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        amplitude: float = 0
        for i in range(4):
            amplitude += (self.__alpha[i] * pow(ippGeomagneticLatitude, i))
        if amplitude < 0:
            amplitude = 0
        return amplitude

    @property
    def __ionosphericDelayPeriod(self) -> float:
        ippGeomagneticLatitude: float = self.__ippGeomagneticLatitude
        period: float = 0
        for i in range(4):
            period += (self.__beta[i] * pow(ippGeomagneticLatitude, i))
        if period < 72_000:
            period = 72_000
        return period

    @property
    def __ionosphericDelayPhase(self) -> float:
        ippLocalTime: float = self.__ippLocalTime
        ionosphericDelayPeriod: float = self.__ionosphericDelayPeriod
        ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
        return ionosphericDelayPhase

    @property
    def __slantFactor(self) -> float:
        slantFactor: float = 1.0 + 16.0 * math.pow((0.53 - self.__elevationAngle), 3)
        return slantFactor

    @property
    def __klobucharDelayInSeconds(self) -> float:
        ionosphericDelayPhase: float = self.__ionosphericDelayPhase
        ionosphericDelayAmplitude: float = self.__ionosphericDelayAmplitude
        slantFactor: float = self.__slantFactor
        if math.fabs(ionosphericDelayPhase) > 1.57:
            ionosphericTimeDelay = 5E-9 * slantFactor
        else:
            ionosphericTimeDelay = (5E-9 +
                                    ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                                 math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
        return ionosphericTimeDelay


class IonosphericDelaysFactory:
    __lonFirst: int = -180
    __dlon: int = 5
    __tecValuesPerLine: int = 16

    def __init__(self, weightMatrix: WeightMatrix, lon1: int, lon2: int, lon3: int, lon4: int,
                 amountOfObservations: int):
        self.__amountOfObservations: int = amountOfObservations
        self.__weightMatrix: WeightMatrix = weightMatrix
        self.__lon1: int = lon1
        self.__lon2: int = lon2
        self.__lon3: int = lon3
        self.__lon4: int = lon4

    def createDelays(self, tecA1: list, tecA2: list, tecA3: list, tecA4: list) -> list:
        delays: list = []
        self.__setRows()
        self.__setPos()

        for observation in range(self.__amountOfObservations):
            a1: int = tecA1[observation][self.__rowA1][self.__posA1]
            a2: int = tecA2[observation][self.__rowA2][self.__posA2]
            a3: int = tecA3[observation][self.__rowA3][self.__posA3]
            a4: int = tecA4[observation][self.__rowA4][self.__posA4]

            tecArray: list = []
            tecArray.extend([a1, a2, a3, a4])

            tec: Tec = Tec(tecArray)
            delay: IonosphericDelay = IonosphericDelay(self.__weightMatrix, tec)
            delays.append(delay)
        return delays

    def __setPos(self) -> None:
        self.__posA1 = self.__getPos(self.__rowA1, self.__lon1)
        self.__posA2 = self.__getPos(self.__rowA2, self.__lon2)
        self.__posA3 = self.__getPos(self.__rowA3, self.__lon3)
        self.__posA4 = self.__getPos(self.__rowA4, self.__lon4)

    def __getPos(self, row: int, lon: int) -> int:
        number: int = int(abs(((self.__lonFirst - lon) / self.__dlon)) - (row * self.__tecValuesPerLine))
        return number

    def __setRows(self) -> None:
        self.__rowA1 = self.__getRow(self.__lon1)
        self.__rowA2 = self.__getRow(self.__lon2)
        self.__rowA3 = self.__getRow(self.__lon3)
        self.__rowA4 = self.__getRow(self.__lon4)

    def __getRow(self, lon: int) -> int:
        row: int = int(abs((self.__lonFirst - lon) / (self.__tecValuesPerLine * self.__dlon)))
        return row


class KlobucharDelaysFactory:
    def __init__(self, latpp: float, lonpp: float, gpsTime: GpsTime, alpha: IonCoefficients, beta: IonCoefficients,
                 amountOfObservations: int) -> None:
        self.__latpp: float = latpp
        self.__lonpp: float = lonpp
        self.__gpsTime: GpsTime = gpsTime
        self.__alpha: IonCoefficients = alpha
        self.__beta: IonCoefficients = beta
        self.__amountOfObservations: int = amountOfObservations

    def createKlobuchar(self) -> list:
        models: list = []
        for observation in range(self.__amountOfObservations):
            time: float = self.__gpsTime[observation]
            model: KlobucharModel = KlobucharModel(self.__latpp, self.__lonpp, time, self.__alpha, self.__beta)
            models.append(model)
        return models


class EphemerisFileReader:
    def __init__(self, amountOfObservations: int, fileName: str) -> None:
        self.__amountOfObservations: int = amountOfObservations
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()
        self.__allLines = self.__analyzeSyntaxAndReturnLinesList()

    @property
    def alpha(self) -> list:
        alpha: list = self.__extractIonCoefficients(3)
        return alpha

    @property
    def beta(self) -> list:
        beta: list = self.__extractIonCoefficients(4)
        return beta

    def __extractIonCoefficients(self, lineNumber: int) -> list:
        line: list = self.__allLines[lineNumber]
        coefficients: list = []
        for coefficient in range(4):
            numeric: float = self.__getCoefficientNumeric(line, coefficient)
            coefficients.append(numeric)
        return coefficients

    @classmethod
    def __getCoefficientNumeric(cls, line: list, number: int) -> float:
        numberBuilder: str = ""
        digits: int = len(line[number])
        for digit in range(digits):
            symbol: chr = chr(line[number][digit])
            if symbol == 'D':
                numberBuilder += 'E'
            else:
                numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def getGpsTime(self, requiredSatelliteNumber: int) -> list:
        if requiredSatelliteNumber < 10:
            requiredSatelliteNumberInAscii: int = self.__toAscii(requiredSatelliteNumber)
            return self.__getGpsTimeArr(requiredSatelliteNumberInAscii, 1)
        else:
            satelliteNumberFirstDigit: int = int(str(requiredSatelliteNumber)[0])
            satelliteNumberSecondDigit: int = int(str(requiredSatelliteNumber)[1])

            satelliteNumberFirstDigitInAscii: int = self.__toAscii(satelliteNumberFirstDigit)
            satelliteNumberSecondDigitInAscii: int = self.__toAscii(satelliteNumberSecondDigit)

            return self.__getGpsTimeArr(satelliteNumberFirstDigitInAscii, 2, satelliteNumberSecondDigitInAscii)

    def __getGpsTimeArr(self, requiredSatelliteNumberFirstDigit: int, requiredSatelliteNumberSize: int,
                        requiredSatelliteNumberSecondDigit: int = None) -> list:
        gpsTime: list = [0] * self.__amountOfObservations
        startOfObservations: int = 8
        linesPerObservations: int = 8
        asciiStarts: int = 48

        for observation in range(startOfObservations, len(self.__allLines), linesPerObservations):
            try:
                minutesFirstNumber: int = self.__allLines[observation][5][0] - asciiStarts
                minutesNumberSize: int = len(self.__allLines[observation][5])
                if minutesFirstNumber == 0 and minutesNumberSize == 1:
                    if requiredSatelliteNumberSecondDigit is not None:
                        satelliteNumberFirstDigit: int = self.__allLines[observation][0][0]
                        satelliteNumberSecondDigit: int = self.__allLines[observation][0][1]
                        satelliteNumberSize = len(self.__allLines[observation][0])
                        hourFirstNumber: int = self.__allLines[observation][4][0] - asciiStarts
                        if (len(self.__allLines[observation][4]) == 1 and
                                satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                satelliteNumberSecondDigit is requiredSatelliteNumberSecondDigit and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = self.__getGpsTimeNumeric(observation)
                            gpsTime[int(hourFirstNumber / 2)] = numeric
                        else:
                            hourSecondNumber: int = self.__allLines[observation][4][1] - asciiStarts
                            hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                            hourValue: int = int(hourFull)

                            if (satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                    satelliteNumberSecondDigit is requiredSatelliteNumberSecondDigit and
                                    satelliteNumberSize is requiredSatelliteNumberSize):
                                numeric: float = self.__getGpsTimeNumeric(observation)
                                gpsTime[int(hourValue / 2)] = numeric
                    else:
                        satelliteNumberFirstDigit: int = self.__allLines[observation][0][0]
                        satelliteNumberSize = len(self.__allLines[observation][0])
                        hourFirstNumber: int = self.__allLines[observation][4][0] - asciiStarts
                        if (len(self.__allLines[observation][4]) == 1 and
                                satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = self.__getGpsTimeNumeric(observation)
                            gpsTime[int(hourFirstNumber / 2)] = numeric
                        else:
                            hourSecondNumber: int = self.__allLines[observation][4][1] - asciiStarts
                            hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                            hourValue: int = int(hourFull)

                            if (satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                    satelliteNumberSize is requiredSatelliteNumberSize):
                                numeric: float = self.__getGpsTimeNumeric(observation)
                                gpsTime[int(hourValue / 2)] = numeric
            except Exception:
                pass
        return gpsTime

    @classmethod
    def __toAscii(cls, number: int) -> int:
        return number + 48

    def __getGpsTimeNumeric(self, observation: int) -> float:
        numberBuilder: str = ""
        observationOffset: int = 7
        digits: int = len(self.__allLines[observation + observationOffset][0])
        for digit in range(digits):
            symbol: chr = chr(self.__allLines[observation + observationOffset][0][digit])
            if symbol == 'D':
                numberBuilder += 'E'
            else:
                numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        allLines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        newLine: int = 10
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                allLines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is space:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space:
                isWord = True
                symbols.append(symbol)
        return allLines


class IonoFileReader:
    def __init__(self, fileName: str) -> None:
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()
        self.__allLines = self.__analyzeSyntaxAndReturnLinesList()

    def getTecArray(self, requiredLat: float, firstLine: int) -> list:
        tecArray: list = []
        amountOfLinesWithTec: int = firstLine + 5575

        requiredFirstLatDigit: int = ord(str(requiredLat)[0])
        requiredSecondLatDigit: int = ord(str(requiredLat)[1])
        requiredThirdLatDigit: int = ord(str(requiredLat)[2])

        for line in range(firstLine, amountOfLinesWithTec):
            try:
                firstDigitOfLat: int = self.__allLines[line][0][0]
                secondDigitOfLat: int = self.__allLines[line][0][1]
                thirdDigitOfLat: int = self.__allLines[line][0][2]
                linesWithTecPerLat: int = 5
                if (firstDigitOfLat is requiredFirstLatDigit and
                        secondDigitOfLat is requiredSecondLatDigit and
                        thirdDigitOfLat is requiredThirdLatDigit):
                    tecPerLat: list = []
                    for lineWithTec in range(1, linesWithTecPerLat + 1):
                        numbersLine: list = self.__getNumberLine(line, lineWithTec)
                        tecPerLat.append(numbersLine)
                    tecArray.append(tecPerLat)
            except Exception:
                pass
        return tecArray

    @classmethod
    def __toAscii(cls, number: str) -> int:
        return int(number) + 48

    def __getNumberLine(self, line: int, lineWithTec: int) -> list:
        numbersLine: list = []
        numbersInRow: int = len(self.__allLines[line + lineWithTec])
        for number in range(numbersInRow):
            numeric: int = self.__getNumeric(line, lineWithTec, number)
            numbersLine.append(numeric)
        return numbersLine

    def __getNumeric(self, line: int, lineWithTec: int, number: int) -> int:
        numberBuilder: str = ""
        numberLength: int = len(self.__allLines[line + lineWithTec][number])
        for digit in range(numberLength):
            symbol: chr = chr(self.__allLines[line + lineWithTec][number][digit])
            numberBuilder += symbol
        numeric: int = int(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        allLines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        newLine: int = 10
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                allLines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is space:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space:
                isWord = True
                symbols.append(symbol)
        return allLines


class ConsoleOutput:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def printDelays(self) -> None:
        print("igrg\tigsg\tklobuchar")
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" +
                  str(round(klobucharValue, 3)))


class GraphDrawer:
    def __init__(self, forecastDelays: list, preciseDelays: list, klobucharDelays: list,
                 amountOfObservations: int) -> None:
        self.__forecastDelays: list = forecastDelays
        self.__preciseDelays: list = preciseDelays
        self.__klobucharDelays: list = klobucharDelays
        self.__amountOfObservations: int = amountOfObservations

    def showDelays(self) -> None:
        forecastValues: list = []
        preciseValues: list = []
        klobucharValues: list = []
        observations = np.arange(0, self.__amountOfObservations)
        for observation in range(self.__amountOfObservations):
            forecastValue: float = self.__forecastDelays[observation].delayInMeters
            preciseValue: float = self.__preciseDelays[observation].delayInMeters
            klobucharValue: float = self.__klobucharDelays[observation].klobucharDelayInMeters
            forecastValues.append(forecastValue)
            preciseValues.append(preciseValue)
            klobucharValues.append(klobucharValue)
        plt.plot(observations * 2, forecastValues, 'o-', label="igrg")
        plt.plot(observations * 2, preciseValues, 'o-', label="igsg")
        plt.plot(observations * 2, klobucharValues, 'o-', label="Klobuchar")
        plt.locator_params(axis='x', nbins=24)
        plt.xlabel("Время, час")
        plt.ylabel("Ионосферная поправка, метр")
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()


def main():
    amountOfObservations: int = 12

    forecastListFirstLine: int = 304
    preciseListFirstLine: int = 394

    satelliteNumber: int = 7

    latpp: float = %s
    lonpp: float = %s
    userGeo: UserGeo = UserGeo(latpp, lonpp)

    lat1: float = %s
    lat2: float = %s
    lon1: int = %s
    lon2: int = %s
    igpGeo: IgpGeo = IgpGeo(lon1, lon2, lat1, lat2)

    axisGeo: AxisGeo = AxisGeo(userGeo, igpGeo)
    weightMatrix: WeightMatrix = WeightMatrix(axisGeo)

    fileNameEphemeris: str = "resources/brdc0010.18n"
    fileReaderEphemeris: EphemerisFileReader = EphemerisFileReader(amountOfObservations, fileNameEphemeris)
    alpha: IonCoefficients = IonCoefficients(fileReaderEphemeris.alpha)
    beta: IonCoefficients = IonCoefficients(fileReaderEphemeris.beta)
    gpsTime: GpsTime = GpsTime(fileReaderEphemeris.getGpsTime(satelliteNumber))

    fileNameForecast: str = "resources/igrg0010.18i"
    ionoFileReaderForecast: IonoFileReader = IonoFileReader(fileNameForecast)

    forecastA1: list = ionoFileReaderForecast.getTecArray(lat2, forecastListFirstLine)
    forecastA2: list = ionoFileReaderForecast.getTecArray(lat2, forecastListFirstLine)
    forecastA3: list = ionoFileReaderForecast.getTecArray(lat1, forecastListFirstLine)
    forecastA4: list = ionoFileReaderForecast.getTecArray(lat1, forecastListFirstLine)

    fileNamePrecise: str = "resources/igsg0010.18i"
    ionoFileReaderPrecise: IonoFileReader = IonoFileReader(fileNamePrecise)

    preciseA1: list = ionoFileReaderPrecise.getTecArray(lat2, preciseListFirstLine)
    preciseA2: list = ionoFileReaderPrecise.getTecArray(lat2, preciseListFirstLine)
    preciseA3: list = ionoFileReaderPrecise.getTecArray(lat1, preciseListFirstLine)
    preciseA4: list = ionoFileReaderPrecise.getTecArray(lat1, preciseListFirstLine)

    ionosphericDelaysFactory: IonosphericDelaysFactory = IonosphericDelaysFactory(weightMatrix, lon2, lon1, lon1, lon2,
                                                                                  amountOfObservations)
    klobucharDelaysFactory: KlobucharDelaysFactory = KlobucharDelaysFactory(latpp, lonpp, gpsTime, alpha, beta,
                                                                            amountOfObservations)

    forecastDelays: list = ionosphericDelaysFactory.createDelays(forecastA1, forecastA2, forecastA3, forecastA4)
    preciseDelays: list = ionosphericDelaysFactory.createDelays(preciseA1, preciseA2, preciseA3, preciseA4)
    klobucharDelays: list = klobucharDelaysFactory.createKlobuchar()

    consoleOutput: ConsoleOutput = ConsoleOutput(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    consoleOutput.printDelays()

    graphDrawer: GraphDrawer = GraphDrawer(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    graphDrawer.showDelays()


if __name__ == "__main__":
    main()
