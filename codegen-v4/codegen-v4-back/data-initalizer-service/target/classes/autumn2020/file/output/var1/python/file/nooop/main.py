import matplotlib.pyplot as plt
import numpy as np


def readBytes(fileName: str) -> bytes:
    with open(fileName, "rb") as file:
        allBytes: bytes = file.read()
    return allBytes


def analyzeSyntaxAndReturnLinesList(allBytes: bytes) -> list:
    allLines: list = []
    words: list = []
    symbols: list = []
    isWord: bool = False

    tab: int = 9
    newLine: int = 10
    carriageReturn: int = 13
    space: int = 32

    for symbol in allBytes:
        if symbol is newLine:
            words.append(symbols)
            symbols = []
            allLines.append(words)
            words = []
            isWord = False
        elif isWord is True and symbol is tab:
            words.append(symbols)
            symbols = []
            isWord = False
        elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
            isWord = True
            symbols.append(symbol)
    return allLines


def __getMeasurements(allLines: list, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                      requiredSatelliteNumber2: int = None) -> list:
    measurements: list = []
    numbersInLine: int = 21
    for line in allLines:
        try:
            satelliteNumber: int = line[0][0]
            satelliteNumberSize: int = len(line[0])
            if requiredSatelliteNumber2 is not None:
                satelliteNumber2: int = line[0][1]
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumber2 is requiredSatelliteNumber2 and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
            else:
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
        except Exception:
            pass
    return measurements


def __getNumeric(line: list, number: int) -> float:
    numberBuilder: str = ""
    numberLength: int = len(line[number])
    for digit in range(numberLength):
        symbol: chr = chr(line[number][digit])
        numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def getDelays(allLines: list, amountOfObservations: int, requiredSatelliteNumber: int) -> list:
    if requiredSatelliteNumber < 10:
        satelliteNumberInAscii: int = __toAscii(requiredSatelliteNumber)
        measurements: list = __getMeasurements(allLines, satelliteNumberInAscii, 1)
    else:
        satelliteNumberFirstDigit: int = int(str(requiredSatelliteNumber)[0])
        satelliteNumberSecondDigit: int = int(str(requiredSatelliteNumber)[1])

        satelliteNumberFirstDigitInAscii = __toAscii(satelliteNumberFirstDigit)
        satelliteNumberSecondDigitInAscii = __toAscii(satelliteNumberSecondDigit)

        measurements: list = __getMeasurements(allLines, satelliteNumberFirstDigitInAscii, 2,
                                               satelliteNumberSecondDigitInAscii)

    speedOfLight: float = 2.99792458 * 1E8
    delays: list = []
    for observation in range(amountOfObservations):
        p1: float = measurements[observation][1]
        p2: float = measurements[observation][2]
        k: float = __getK()
        delay: float = (p1 - p2) / (speedOfLight * (1 - k))
        delayInMeters: float = delay * speedOfLight
        delays.append(delayInMeters)
    return delays


def __toAscii(number: int) -> int:
    return number + 48


def __getK() -> float:
    f1: float = 1_575_420_000
    f2: float = 1_227_600_000
    k: float = pow(f1, 2) / pow(f2, 2)
    return k


def printDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int, delays1: list, delays2: list,
                delays3: list, amountOfObservations: int) -> None:
    print("Ионосферная задержка\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
          "\t\tСпутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        delay1: float = delays1[observation]
        delay2: float = delays2[observation]
        delay3: float = delays3[observation]
        print(str(round(delay1, 10)) + "\t" + str(round(delay2, 10)) + "\t" + str(round(delay3, 10)))


def showDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int, delays1: list, delays2: list,
               delays3: list, amountOfObservations: int) -> None:
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations, delays1, 'o-', label="Cпутник #" + str(satellite1Number))
    plt.plot(observations, delays2, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, delays3, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Ионосферная задержка, метры")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 360

    satellite1Number: int = %s
    satellite2Number: int = %s
    satellite3Number: int = %s

    fileName: str = "../resources/file.dat"
    allBytes: bytes = readBytes(fileName)
    allLines: list = analyzeSyntaxAndReturnLinesList(allBytes)

    satellite1Delays: list = getDelays(allLines, amountOfObservations, satellite1Number)
    satellite2Delays: list = getDelays(allLines, amountOfObservations, satellite2Number)
    satellite3Delays: list = getDelays(allLines, amountOfObservations, satellite3Number)

    printDelays(satellite1Number, satellite2Number, satellite3Number,
                satellite1Delays, satellite2Delays, satellite3Delays,
                amountOfObservations)

    showDelays(satellite1Number, satellite2Number, satellite3Number,
               satellite1Delays, satellite2Delays, satellite3Delays,
               amountOfObservations)


if __name__ == '__main__':
    main()
