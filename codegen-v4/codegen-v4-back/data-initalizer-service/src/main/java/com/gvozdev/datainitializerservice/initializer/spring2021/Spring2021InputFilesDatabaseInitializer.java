package com.gvozdev.datainitializerservice.initializer.spring2021;

import com.gvozdev.datainitializerservice.DataInitializerServiceApplication;
import com.gvozdev.datainitializerservice.entity.spring2021.Spring2021InputFile;
import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.gvozdev.datainitializerservice.repository.spring2021.Spring2021InputFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static com.gvozdev.datainitializerservice.initializer.path.ResourcePaths.SPRING2021_INPUT_FILE;
import static java.nio.file.Files.readAllBytes;
import static java.text.MessageFormat.format;
import static java.util.Objects.requireNonNull;

@Slf4j
@Component
public class Spring2021InputFilesDatabaseInitializer implements Initializer {
    private final Spring2021InputFileRepository inputFileRepository;

    public Spring2021InputFilesDatabaseInitializer(Spring2021InputFileRepository inputFileRepository) {
        this.inputFileRepository = inputFileRepository;
    }

    @Override
    @Transactional
    public void initialize() {
        saveInputFile(1L, "arti_6hours.dat");
        saveInputFile(2L, "bshm_1-10.dat");
        saveInputFile(3L, "gele_6hours.dat");
        saveInputFile(4L, "hueg_1-10.dat");
        saveInputFile(5L, "irkm_6hours.dat");
        saveInputFile(6L, "kslv_6hours.dat");
        saveInputFile(7L, "leij_1-10.dat");
        saveInputFile(8L, "maga_6hours.dat");
        saveInputFile(9L, "menp_6hours.dat");
        saveInputFile(10L, "novs_6hours.dat");
        saveInputFile(11L, "noyb_6hours.dat");
        saveInputFile(12L, "onsa_1-10.dat");
        saveInputFile(13L, "spt0_1-10.dat");
        saveInputFile(14L, "svet_6hours.dat");
        saveInputFile(15L, "svtl_1-10.dat");
        saveInputFile(16L, "tit2_1-10.dat");
        saveInputFile(17L, "vis0_1-10.dat");
        saveInputFile(18L, "vlad_6hours.dat");
        saveInputFile(19L, "warn_1-10.dat");
        saveInputFile(20L, "ykts_6hours.dat");
        saveInputFile(21L, "yusa_6hours.dat");
        saveInputFile(22L, "zeck_1-10.dat");
        saveInputFile(23L, "resources.rar");

        log.info("Все входные файлы для УИРС весны 2021 успешно загружены");
    }

    private void saveInputFile(long id, String name) {
        try {
            String path = format(SPRING2021_INPUT_FILE, name);

            URI uri = requireNonNull(DataInitializerServiceApplication.class.getClassLoader().getResource(path)).toURI();
            File file = new File(uri);
            byte[] fileBytes = readAllBytes(file.toPath());
            Spring2021InputFile inputFile = new Spring2021InputFile(id, name, fileBytes);

            inputFileRepository.save(inputFile);
        } catch (IOException | URISyntaxException exception) {
            exception.printStackTrace();
        }
    }
}
