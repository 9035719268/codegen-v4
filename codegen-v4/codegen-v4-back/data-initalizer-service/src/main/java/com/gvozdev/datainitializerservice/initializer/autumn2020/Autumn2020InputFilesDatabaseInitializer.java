package com.gvozdev.datainitializerservice.initializer.autumn2020;

import com.gvozdev.datainitializerservice.DataInitializerServiceApplication;
import com.gvozdev.datainitializerservice.entity.autumn2020.Autumn2020InputFile;
import com.gvozdev.datainitializerservice.initializer.api.Initializer;
import com.gvozdev.datainitializerservice.repository.autumn2020.Autumn2020InputFileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static com.gvozdev.datainitializerservice.initializer.path.ResourcePaths.AUTUMN2020_INPUT_FILE;
import static java.nio.file.Files.readAllBytes;
import static java.text.MessageFormat.format;
import static java.util.Objects.requireNonNull;

@Slf4j
@Component
public class Autumn2020InputFilesDatabaseInitializer implements Initializer {
    private final Autumn2020InputFileRepository inputFileRepository;

    public Autumn2020InputFilesDatabaseInitializer(Autumn2020InputFileRepository inputFileRepository) {
        this.inputFileRepository = inputFileRepository;
    }

    @Override
    @Transactional
    public void initialize() {
        saveInputFile(1L, "BOGI_1-6.dat");
        saveInputFile(2L, "brdc0010.18n");
        saveInputFile(3L, "bshm_1-10.dat");
        saveInputFile(4L, "HUEG_1-6.dat");
        saveInputFile(5L, "hueg_1-10.dat");
        saveInputFile(6L, "igrg0010.18i");
        saveInputFile(7L, "igsg0010.18i");
        saveInputFile(8L, "leij_1-10.dat");
        saveInputFile(9L, "ONSA_1-6.dat");
        saveInputFile(10L, "onsa_1-10.dat");
        saveInputFile(11L, "POTS_6hours.dat");
        saveInputFile(12L, "spt0_1-10.dat");
        saveInputFile(13L, "svtl_1-10.dat");
        saveInputFile(14L, "tit2_1-10.dat");
        saveInputFile(15L, "TITZ_6hours.dat");
        saveInputFile(16L, "vis0_1-10.dat");
        saveInputFile(17L, "warn_1-10.dat");
        saveInputFile(18L, "WARN_6hours.dat");
        saveInputFile(19L, "zeck_1-10.dat");
        saveInputFile(20L, "resources.rar");
        saveInputFile(21L, "reference.pdf");

        log.info("Все входные файлы для УИРС осени 2020 успешно загружены");
    }

    private void saveInputFile(long id, String name) {
        try {
            String path = format(AUTUMN2020_INPUT_FILE, name);

            URI uri = requireNonNull(DataInitializerServiceApplication.class.getClassLoader().getResource(path)).toURI();
            File file = new File(uri);
            byte[] fileBytes = readAllBytes(file.toPath());
            Autumn2020InputFile inputFile = new Autumn2020InputFile(id, name, fileBytes);

            inputFileRepository.save(inputFile);
        } catch (URISyntaxException | IOException exception) {
            exception.printStackTrace();
        }
    }
}
