package com.gvozdev.datainitializerservice.repository.spring2021;

import com.gvozdev.datainitializerservice.entity.spring2021.Spring2021InputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Spring2021InputFileRepository extends JpaRepository<Spring2021InputFile, Long> {
    Spring2021InputFile findByName(String name);
}
