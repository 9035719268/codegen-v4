import math
import matplotlib.pyplot as plt
import numpy as np


def readBytes(fileName: str) -> bytes:
    with open(fileName, "rb") as file:
        allBytes: bytes = file.read()
    return allBytes


def analyzeSyntaxAndReturnLinesList(allBytes: bytes) -> list:
    allLines: list = []
    words: list = []
    symbols: list = []
    isWord: bool = False

    newLine: int = 10
    space: int = 32

    for symbol in allBytes:
        if symbol is newLine:
            words.append(symbols)
            symbols = []
            allLines.append(words)
            words = []
            isWord = False
        elif isWord is True and symbol is space:
            words.append(symbols)
            symbols = []
            isWord = False
        elif symbol is not space:
            isWord = True
            symbols.append(symbol)
    return allLines


def extractIonCoefficients(allLines: list, lineNumber: int) -> list:
    line: list = allLines[lineNumber]
    coefficients: list = []
    for coefficient in range(4):
        numeric: float = __getCoefficientNumeric(line, coefficient)
        coefficients.append(numeric)
    return coefficients


def __getCoefficientNumeric(line: list, number: int) -> float:
    numberBuilder: str = ""
    digits: int = len(line[number])
    for digit in range(digits):
        symbol: chr = chr(line[number][digit])
        if symbol == 'D':
            numberBuilder += 'E'
        else:
            numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def getGpsTime(allLines: list, amountOfObservations: int, requiredSatelliteNumber: int) -> list:
    if requiredSatelliteNumber < 10:
        requiredSatelliteNumberInAscii: int = __toAscii(requiredSatelliteNumber)
        return __getGpsTimeArr(allLines, amountOfObservations, requiredSatelliteNumberInAscii, 1)
    else:
        satelliteNumberFirstDigit: int = int(str(requiredSatelliteNumber)[0])
        satelliteNumberSecondDigit: int = int(str(requiredSatelliteNumber)[1])

        satelliteNumberFirstDigitInAscii: int = __toAscii(satelliteNumberFirstDigit)
        satelliteNumberSecondDigitInAscii: int = __toAscii(satelliteNumberSecondDigit)

        return __getGpsTimeArr(allLines, amountOfObservations, satelliteNumberFirstDigitInAscii, 2,
                               satelliteNumberSecondDigitInAscii)


def __getGpsTimeArr(allLines: list, amountOfObservations: int, requiredSatelliteNumberFirstDigit: int,
                    requiredSatelliteNumberSize: int, requiredSatelliteNumberSecondDigit: int = None) -> list:
    gpsTime: list = [0] * amountOfObservations
    startOfObservations: int = 8
    linesPerObservations: int = 8
    asciiStarts: int = 48

    for observation in range(startOfObservations, len(allLines), linesPerObservations):
        try:
            minutesFirstNumber: int = allLines[observation][5][0] - asciiStarts
            minutesNumberSize: int = len(allLines[observation][5])
            if minutesFirstNumber == 0 and minutesNumberSize == 1:
                if requiredSatelliteNumberSecondDigit is not None:
                    satelliteNumberFirstDigit: int = allLines[observation][0][0]
                    satelliteNumberSecondDigit: int = allLines[observation][0][1]
                    satelliteNumberSize = len(allLines[observation][0])
                    hourFirstNumber: int = allLines[observation][4][0] - asciiStarts
                    if (len(allLines[observation][4]) == 1 and
                            satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                            satelliteNumberSecondDigit is requiredSatelliteNumberSecondDigit and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        numeric: float = __getGpsTimeNumeric(allLines, observation)
                        gpsTime[int(hourFirstNumber / 2)] = numeric
                    else:
                        hourSecondNumber: int = allLines[observation][4][1] - asciiStarts
                        hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                        hourValue: int = int(hourFull)

                        if (satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                satelliteNumberSecondDigit is requiredSatelliteNumberSecondDigit and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = __getGpsTimeNumeric(allLines, observation)
                            gpsTime[int(hourValue / 2)] = numeric
                else:
                    satelliteNumberFirstDigit: int = allLines[observation][0][0]
                    satelliteNumberSize = len(allLines[observation][0])
                    hourFirstNumber: int = allLines[observation][4][0] - asciiStarts
                    if (len(allLines[observation][4]) == 1 and
                            satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        numeric: float = __getGpsTimeNumeric(allLines, observation)
                        gpsTime[int(hourFirstNumber / 2)] = numeric
                    else:
                        hourSecondNumber: int = allLines[observation][4][1] - asciiStarts
                        hourFull: str = str(hourFirstNumber) + str(hourSecondNumber)
                        hourValue: int = int(hourFull)

                        if (satelliteNumberFirstDigit is requiredSatelliteNumberFirstDigit and
                                satelliteNumberSize is requiredSatelliteNumberSize):
                            numeric: float = __getGpsTimeNumeric(allLines, observation)
                            gpsTime[int(hourValue / 2)] = numeric
        except Exception:
            pass
    return gpsTime


def __getGpsTimeNumeric(allLines: list, observation: int) -> float:
    numberBuilder: str = ""
    observationOffset: int = 7
    digits = len(allLines[observation + observationOffset][0])

    for digit in range(digits):
        symbol: chr = chr(allLines[observation + observationOffset][0][digit])
        if symbol == 'D':
            numberBuilder += 'E'
        else:
            numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def __toAscii(number: int) -> int:
    return number + 48


def getTecArray(allLines: list, requiredLat: float, firstLine: int) -> list:
    tecArray: list = []
    amountOfLinesWithTec: int = firstLine + 5575

    requiredFirstLatDigit: int = ord(str(requiredLat)[0])
    requiredSecondLatDigit: int = ord(str(requiredLat)[1])
    requiredThirdLatDigit: int = ord(str(requiredLat)[2])

    for line in range(firstLine, amountOfLinesWithTec):
        try:
            firstDigitOfLat: int = allLines[line][0][0]
            secondDigitOfLat: int = allLines[line][0][1]
            thirdDigitOfLat: int = allLines[line][0][2]
            linesWithTecPerLat: int = 5
            if (firstDigitOfLat is requiredFirstLatDigit and
                    secondDigitOfLat is requiredSecondLatDigit and
                    thirdDigitOfLat is requiredThirdLatDigit):
                tecPerLat: list = []
                for lineWithTec in range(1, linesWithTecPerLat + 1):
                    numbersLine: list = __getNumberLine(allLines, line, lineWithTec)
                    tecPerLat.append(numbersLine)
                tecArray.append(tecPerLat)
        except Exception:
            pass
    return tecArray


def __getNumberLine(allLines: list, line: int, lineWithTec: int) -> list:
    numbersLine: list = []
    numbersInRow: int = len(allLines[line + lineWithTec])
    for number in range(numbersInRow):
        numeric: int = __getNumeric(allLines, line, lineWithTec, number)
        numbersLine.append(numeric)
    return numbersLine


def __getNumeric(allLines: list, line: int, lineWithTec: int, number: int) -> int:
    numberBuilder: str = ""
    numberLength: int = len(allLines[line + lineWithTec][number])
    for digit in range(numberLength):
        symbol: chr = chr(allLines[line + lineWithTec][number][digit])
        numberBuilder += symbol
    numeric: int = int(numberBuilder)
    return numeric


def __getDelayInMeters(weightMatrix: list, tec: list) -> float:
    tecuToMetersCoefficient: float = __getTecuToMetersCoefficient()
    delayInTecu: float = __getDelayInTecu(weightMatrix, tec)
    delayInMeters: float = delayInTecu * tecuToMetersCoefficient
    return delayInMeters


def __getTecuToMetersCoefficient():
    l1: float = 1_575_420_000
    oneTecUnit: float = 1E16
    coefficient: float = 40.3 / pow(l1, 2) * oneTecUnit
    return coefficient


def __getDelayInTecu(weightMatrix: list, tecList: list) -> float:
    delay: float = 0
    for observation in range(4):
        weight: float = weightMatrix[observation]
        rawTec: float = tecList[observation]
        tecInOneTecUnit: float = rawTec * 0.1
        delay += (weight * tecInOneTecUnit)
    return delay


def __getEarthCenteredAngle(elevationAngle: float) -> float:
    earthCenteredAngle: float = 0.0137 / (elevationAngle + 0.11) - 0.022
    return earthCenteredAngle


def __getIppLatitude(latpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = latpp + earthCenteredAngle * math.cos(azimuth)
    if ippLatitude > 0.416:
        ippLatitude = 0.416
    elif ippLatitude < -0.416:
        ippLatitude = -0.416
    return ippLatitude


def __getIppLongtitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    earthCenteredAngle: float = __getEarthCenteredAngle(elevationAngle)
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = lonpp + (earthCenteredAngle * math.sin(azimuth) / (math.cos(ippLatitude)))
    return ippLongtitude


def __getIppGeomagneticLatitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float) -> float:
    ippLatitude: float = __getIppLatitude(latpp, elevationAngle, azimuth)
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippGeomagneticLatitude: float = ippLatitude + 0.064 * math.cos(ippLongtitude - 1.617)
    return ippGeomagneticLatitude


def __getIppLocalTime(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float) -> float:
    secondsInOneDay: float = 86_400
    secondsInTwelveHours: float = 43_200
    ippLongtitude: float = __getIppLongtitude(latpp, lonpp, elevationAngle, azimuth)
    ippLocalTime: float = secondsInTwelveHours * ippLongtitude + gpsTime
    while ippLocalTime > secondsInOneDay:
        ippLocalTime -= secondsInOneDay
    while ippLocalTime < 0:
        ippLocalTime += secondsInOneDay
    return ippLocalTime


def __getIonosphericDelayAmplitude(latpp: float, lonpp: float, elevationAngle: float, azimuth: float,
                                   alpha: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    amplitude: float = 0
    for i in range(4):
        amplitude += (alpha[i] * pow(ippGeomagneticLatitude, i))
    if amplitude < 0:
        amplitude = 0
    return amplitude


def __getIonosphericDelayPeriod(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, beta: list) -> float:
    ippGeomagneticLatitude: float = __getIppGeomagneticLatitude(latpp, lonpp, elevationAngle, azimuth)
    period: float = 0
    for i in range(4):
        period += (beta[i] * pow(ippGeomagneticLatitude, i))
    if period < 72_000:
        period = 72_000
    return period


def __getIonosphericDelayPhase(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                               beta: list) -> float:
    ippLocalTime: float = __getIppLocalTime(latpp, lonpp, elevationAngle, azimuth, gpsTime)
    ionosphericDelayPeriod: float = __getIonosphericDelayPeriod(latpp, lonpp, elevationAngle, azimuth, beta)
    ionosphericDelayPhase: float = 2 * math.pi * (ippLocalTime - 50_400) / ionosphericDelayPeriod
    return ionosphericDelayPhase


def __getSlantFactor(elevationAngle: float) -> float:
    slantFactor: float = 1 + 16 * math.pow((0.53 - elevationAngle), 3)
    return slantFactor


def __getKlobucharDelayInSeconds(latpp: float, lonpp: float,
                                 elevationAngle: float, azimuth: float, gpsTime: float,
                                 alpha: list, beta: list) -> float:
    ionosphericDelayPhase: float = __getIonosphericDelayPhase(latpp, lonpp, elevationAngle, azimuth, gpsTime, beta)
    ionosphericDelayAmplitude: float = __getIonosphericDelayAmplitude(latpp, lonpp, elevationAngle, azimuth, alpha)
    slantFactor: float = __getSlantFactor(elevationAngle)
    ionosphericTimeDelay: float
    if abs(ionosphericDelayPhase) > 1.57:
        ionosphericTimeDelay = 5E-9 * slantFactor
    else:
        ionosphericTimeDelay = (5E-9 +
                                ionosphericDelayAmplitude * (1 - math.pow(ionosphericDelayPhase, 2) / 2 +
                                                             math.pow(ionosphericDelayPhase, 4) / 24)) * slantFactor
    return ionosphericTimeDelay


def getKlobucharDelayInMeters(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, gpsTime: float,
                              alpha: list, beta: list) -> float:
    delayInSeconds: float = __getKlobucharDelayInSeconds(latpp, lonpp, elevationAngle, azimuth, gpsTime, alpha, beta)
    speedOfLight: float = 2.99792458 * 1E8
    delayInMeters: float = delayInSeconds * speedOfLight
    return delayInMeters


def getIonosphericDelays(tecA1: list, tecA2: list, tecA3: list, tecA4: list,
                         lon1: int, lon2: int, lon3: int, lon4: int,
                         weightMatrix: list, amountOfObservations: int) -> list:
    lonFirst: int = -180
    dlon: int = 5
    tecValuesPerLine: int = 16
    delays: list = []

    rowA1: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon1)
    rowA2: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon2)
    rowA3: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon3)
    rowA4: int = __getRow(lonFirst, dlon, tecValuesPerLine, lon4)

    posA1: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA1, lon1)
    posA2: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA2, lon2)
    posA3: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA3, lon3)
    posA4: int = __getPos(lonFirst, dlon, tecValuesPerLine, rowA4, lon4)

    for observation in range(amountOfObservations):
        a1: int = tecA1[observation][rowA1][posA1]
        a2: int = tecA2[observation][rowA2][posA2]
        a3: int = tecA3[observation][rowA3][posA3]
        a4: int = tecA4[observation][rowA4][posA4]

        tec: list = []
        tec.extend([a1, a2, a3, a4])

        delay: float = __getDelayInMeters(weightMatrix, tec)
        delays.append(delay)
    return delays


def __getPos(lonFirst: int, dlon: int, tecValuesPerLine: int, row: int, lon: int) -> int:
    number: int = int(abs(((lonFirst - lon) / dlon)) - (row * tecValuesPerLine))
    return number


def __getRow(lonFirst: int, dlon: int, tecValuesPerLine: int, lon: int) -> int:
    row: int = int(abs((lonFirst - lon) / (tecValuesPerLine * dlon)))
    return row


def getKlobuchar(latpp: float, lonpp: float, elevationAngle: float, azimuth: float, alpha: list, beta: list,
                 gpsTime: list, amountOfObservations: int) -> list:
    models: list = []
    for observation in range(amountOfObservations):
        time: float = gpsTime[observation]
        model: float = getKlobucharDelayInMeters(latpp, lonpp, elevationAngle, azimuth, time, alpha, beta)
        models.append(model)
    return models


def printDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int):
    print("igrg\tigsg\tklobuchar")
    for observation in range(amountOfObservations):
        forecastValue: float = forecastDelays[observation]
        preciseValue: float = preciseDelays[observation]
        klobucharValue: float = klobucharDelays[observation]
        print(str(round(forecastValue, 3)) + "\t" + str(round(preciseValue, 3)) + "\t" + str(round(klobucharValue, 3)))


def showDelays(forecastDelays: list, preciseDelays: list, klobucharDelays: list, amountOfObservations: int):
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations * 2, forecastDelays, 'o-', label="igrg")
    plt.plot(observations * 2, preciseDelays, 'o-', label="igsg")
    plt.plot(observations * 2, klobucharDelays, 'o-', label="Klobuchar")
    plt.locator_params(axis='x', nbins=24)
    plt.xlabel("Время, час")
    plt.ylabel("Ионосферная поправка, метр")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 12

    forecastListFirstLine: int = 304
    preciseListFirstLine: int = 394

    satelliteNumber: int = %s

    halfCircle: float = 180

    latpp: float = %s
    latppInHalfCircles: float = latpp / halfCircle

    lonpp: float = %s
    lonppInHalfCircles: float = lonpp / halfCircle

    lat1: float = %s
    lat1InHalfCircles: float = lat1 / halfCircle

    lat2: float = %s
    lat2InHalfCircles: float = lat2 / halfCircle

    lon1: int = %s
    lon1InHalfCircles: float = lon1 / halfCircle

    lon2: int = %s
    lon2InHalfCircles: float = lon2 / halfCircle

    xpp: float = (lonppInHalfCircles - lon1InHalfCircles) / (lon2InHalfCircles - lon1InHalfCircles)
    ypp: float = (latppInHalfCircles - lat1InHalfCircles) / (lat2InHalfCircles - lat1InHalfCircles)

    weightMatrix: list = [0] * 4
    weightMatrix[0] = xpp * ypp
    weightMatrix[1] = (1 - xpp) * ypp
    weightMatrix[2] = (1 - xpp) * (1 - ypp)
    weightMatrix[3] = xpp * (1 - ypp)

    fileNameEphemeris: str = "resources/brdc0010.18n"
    fileBytesEphemeris: bytes = readBytes(fileNameEphemeris)
    fileLinesEphemeris: list = analyzeSyntaxAndReturnLinesList(fileBytesEphemeris)
    alpha: list = extractIonCoefficients(fileLinesEphemeris, 3)
    beta: list = extractIonCoefficients(fileLinesEphemeris, 4)
    gpsTime: list = getGpsTime(fileLinesEphemeris, amountOfObservations, satelliteNumber)

    fileNameForecast: str = "resources/igrg0010.18i"
    fileBytesForecast: bytes = readBytes(fileNameForecast)
    fileLinesForecast: list = analyzeSyntaxAndReturnLinesList(fileBytesForecast)
    forecastA1: list = getTecArray(fileLinesForecast, lat2, forecastListFirstLine)
    forecastA2: list = getTecArray(fileLinesForecast, lat2, forecastListFirstLine)
    forecastA3: list = getTecArray(fileLinesForecast, lat1, forecastListFirstLine)
    forecastA4: list = getTecArray(fileLinesForecast, lat1, forecastListFirstLine)

    fileNamePrecise: str = "resources/igsg0010.18i"
    fileBytesPrecise: bytes = readBytes(fileNamePrecise)
    fileLinesPrecise: list = analyzeSyntaxAndReturnLinesList(fileBytesPrecise)
    preciseA1: list = getTecArray(fileLinesPrecise, lat2, preciseListFirstLine)
    preciseA2: list = getTecArray(fileLinesPrecise, lat2, preciseListFirstLine)
    preciseA3: list = getTecArray(fileLinesPrecise, lat1, preciseListFirstLine)
    preciseA4: list = getTecArray(fileLinesPrecise, lat1, preciseListFirstLine)

    forecastDelays: list = getIonosphericDelays(forecastA1, forecastA2, forecastA3, forecastA4, lon2, lon1, lon1, lon2,
                                                weightMatrix, amountOfObservations)

    preciseDelays: list = getIonosphericDelays(preciseA1, preciseA2, preciseA3, preciseA4, lon2, lon1, lon1, lon2,
                                               weightMatrix, amountOfObservations)

    elevationAngle: float = 90 / halfCircle
    azimuth: float = 0
    klobucharDelays: list = getKlobuchar(latpp, lonpp, elevationAngle, azimuth, alpha, beta, gpsTime,
                                         amountOfObservations)

    printDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)
    showDelays(forecastDelays, preciseDelays, klobucharDelays, amountOfObservations)


if __name__ == "__main__":
    main()
