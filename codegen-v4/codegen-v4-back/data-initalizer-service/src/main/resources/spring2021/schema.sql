CREATE TABLE IF NOT EXISTS spring_2021_input_file
(
    id         BIGINT PRIMARY KEY,
    name       VARCHAR NOT NULL,
    file_bytes BYTEA   NOT NULL
);

CREATE TABLE IF NOT EXISTS spring_2021_output_file
(
    id            BIGINT PRIMARY KEY,
    lang          VARCHAR NOT NULL,
    file          VARCHAR NOT NULL,
    oop           VARCHAR NOT NULL,
    name          VARCHAR NOT NULL,
    file_bytes    BYTEA   NOT NULL,
    creation_date DATE    NOT NULL
);